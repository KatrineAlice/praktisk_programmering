import sys, time
import numpy as np
from roots import newton
import global_ncalls
import systems
# test of time.time()

#start=time.time()
#print('testing')
#stop=time.time()
#print(stop-start)

print("\nRun test: Newton's method with numerical jacobian ...")

# Test Newtons method with numerical jacobian  on A*x*y = 1
# Initialise
x0 = np.array([2, 1], dtype='float64')
dx = np.array([1e-08, 1e-08], dtype='float64')

# Run test
roots = newton(f=systems.system_A, x0=x0, dx=dx)
print('\nSolve A*x*y = 1, where exp(-x)+exp(-y)=1 + 1/A and A=10000 ...\n')
print('Result:')
print('-------------------------------------------------')
print('x0 = \t\t%s\nf(x0) = \t%s\nroots = \t%s\nf(roots) = \t%s\nnum. of calls = %s' % (x0,
                              systems.system_A(x0),
                              roots,
                              systems.system_A(roots),
                              global_ncalls.ncalls))
print('-------------------------------------------------')
print('\n')


# Test Newtons method on Rosenbrocks valley function
# Initialise
x0 = np.array([1, 10], dtype='float64')
dx = np.array([1e-07, 1e-07], dtype='float64')

# Run test
roots = newton(f=systems.rosenbrock, x0=x0, dx=dx)
print('\nSolve Rosenbrocks valley function ...\n')
print('Result:')
print('-------------------------------------------------')
print('x0 = \t\t%s\nf(x0) = \t%s\nroots = \t%s\nf(roots) = \t%s\nnum. of calls = %s' % (x0,
                              systems.rosenbrock(x0),
                              roots,
                              systems.rosenbrock(roots),
                              global_ncalls.ncalls))
print('-------------------------------------------------')
print('\n')


# Test Newtons method on the Himmelblau function
# Initialise
x0 = np.array([10, 3], dtype='float64')
dx = np.array([1e-07, 1e-07], dtype='float64')

# Run test
roots = newton(f=systems.himmelblau, x0=x0, dx=dx)
print('\nSolve the Himmelblau function ...\n')
print('Result:')
print('-------------------------------------------------')
print('x0 = \t\t%s\nf(x0) = \t%s\nroots = \t%s\nf(roots) = \t%s\nnum. of calls = %s' % (x0,
                              systems.himmelblau(x0),
                              roots,
                              systems.himmelblau(roots),
                              global_ncalls.ncalls))
print('-------------------------------------------------')
print('\n')


# Test Newtons method on some interesting function
