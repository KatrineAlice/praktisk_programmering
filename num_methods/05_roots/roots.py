import sys
sys.path.append('../02_linear_equations')
from lin_solver import gv_decomp
from lin_solver import gv_solver
import numpy as np
import global_ncalls

def newton(f, x0, dx, eps=1e-06):
    """
    Calculates the roots with the newton method with numerical jacobian.
    """
    # Initialise
    global_ncalls.ncalls = 0
    x = x0.copy()
    n = x.size
    J = np.zeros((n, n), dtype='float64')
    fx = f(x)

    # Root finding
    while True:
        global_ncalls.ncalls += 1
        
        # calculate numerical jacobian
        for j in range(n):
            x[j] += dx[j]
            df = f(x) - fx
           
            for i in range(n):
                J[i,j] = df[i]/dx[j]
            
            x[j] -= dx[j]

        # Decompose and solve matrix by Givens rotations
        J = gv_decomp(J)
        Dx = gv_solver(J,-fx)

        # Backtracking linesearch
        alpha = 2.
        while True:
            alpha /= 2
            y = x + Dx*alpha
            fy = f(y)

            fynorm = np.linalg.norm(fy)
            fxnorm = np.linalg.norm(fx)

            if(fynorm < (1-alpha/2)*fxnorm or alpha < 0.02):
                break
        
        # Save the last approximation
        x = y
        fx = fy

        Dxnorm = np.linalg.norm(Dx)
        dxnorm = np.linalg.norm(dx)
        fxnorm = np.linalg.norm(fx)

        if(Dxnorm < dxnorm or fxnorm < eps):
            break
    return x

def newton2(f, jacobian, x0, eps=1e-06):
    """
    Calculates the roots with the newton method with analytical jacobian.
    """
    # Initialise
    global_ncalls.ncalls = 0
    x = x0.copy()
    n = x.size
    J = np.zeros(2, dtype='float64')
    fx = f(x)

    # Root finding
    while True:
        global_ncalls.ncalls += 1
        
        # Calculate jacobian
        # Decompose and solve via Givens rotations
        J = jacobian(x)
        J = gv_decomp(J)
        Dx = gv_solver(J,-fx)

        # Backtracking linesearch:
        alpha = 2.
        while True:
            alpha /= 2
            y = x + Dx*alpha
            fy = f(y)

            fynorm = np.linalg.norm(fy)
            fxnorm = np.linalg.norm(fx)

            if(fynorm < (1-alpha/2)*fxnorm or alpha < 0.03):
                break
        
        # Save the last approximation
        x = y
        fx = fy
        
        fxnorm = np.linalg.norm(fx)

        if(fxnorm < eps):
            break
    
    return x

def newton_quad(f, x0, dx, eps=1e-06):
    """
    Calculates the root with Newton's method using quadratic interpolation and a numerical jacobian.
    """
    # Initialise
    global_ncalls.ncalls = 0
    x = np.copy(x0)
    n = x.size
    J = np.zeros((n, n), dtype='float64')
    fx = f(x)

    # Root finding
    while True:
        global_ncalls.ncalls += 1

        # Calculate numerical Jacobian
        for j in range(n):
            x[j] += dx[j]
            df = f(x) - fx

            for i in range(n):
                J[i,j] = df[i] / dx[j]

            x[j] -= dx[j]
        
        # Decompose and solve matrix by Givens rotations
        J = gv_decomp(J)
        Dx = gv_solver(J,-fx)

        # Quadratic linesearch
        alpha = 1.
        y = x + Dx*alpha
        fy = f(y)

        fxnorm = np.linalg.norm(fx)
        fynorm = np.linalg.norm(fy)

        # Calculate g(0) and g'(0) of the minimization func.
        g0 = 0.5*fxnorm*fxnorm
        dg0 = - fxnorm*fxnorm

        while True:
            galpha = 0.5*fynorm*fynorm
            c = (galpha - g0 - dg0*alpha) / (alpha*alpha)

            # Update
            alpha = - dg0/(2*c)
            y = x + Dx*alpha
            fy = f(y)
            fynorm = np.linalg.norm(fy)

            if (fynorm < (1 - alpha/2)*fxnorm) or (alpha < (1/128)):
                break

        # Save the last approximation
        x = y
        fx = fy

        Dxnorm = np.linalg.norm(Dx)
        dxnorm = np.linalg.norm(dx)

        if Dxnorm < dxnorm or fxnorm < eps:
            break

    return x
