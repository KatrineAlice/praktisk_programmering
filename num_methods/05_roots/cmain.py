import sys, time
import numpy as np
from roots import newton, newton_quad
import global_ncalls
from systems import system_A, rosenbrock, himmelblau, hydrogen
# test of time.time()

#start=time.time()
#print('testing')
#stop=time.time()
#print(stop-start)

print("\nRun test: Newton's method with numerical jacobian and quadratic linesearch ...")

# Test Newtons method with numerical jacobian  on A*x*y = 1
# Initialise
x0 = np.array([0, 1], dtype='float64')
dx = np.array([1e-08, 1e-08], dtype='float64')

# Run test and compare with method from A
roots = newton_quad(f=system_A, x0=x0, dx=dx)
print('\nSolve A*x*y = 1, where exp(-x)+exp(-y)=1 + 1/A and A=10000 ...\n')
print('Result:')
print('-------------------------------------------------')
print(f'x0 = \t\t{x0}')
print(f'f(x0) = \t{system_A(x0)}')
print(f'roots = \t{roots}')
print(f'f(roots) = \t{system_A(roots)}')
print(f'\nnum. of calls = {global_ncalls.ncalls}')
print('-------------------------------------------------')

rootsA = newton(f=system_A, x0=x0, dx=dx)
print('Result with non-quadratic method (Part A):')
print('-------------------------------------------------')
print(f'roots = \t{rootsA}')
print(f'f(roots) = \t{system_A(rootsA)}')
print(f'\nnum. of calls = {global_ncalls.ncalls}')
print('-------------------------------------------------')
print('\n')



# Test Newtons method on Rosenbrocks valley function
# Initialise
x0 = np.array([1, 10], dtype='float64')
dx = np.array([1e-07, 1e-07], dtype='float64')

# Run test
roots = newton_quad(f=rosenbrock, x0=x0, dx=dx)
print('\nSolve Rosenbrocks valley function ...\n')
print('Result:')
print('-------------------------------------------------')
print('x0 = \t\t%s\nf(x0) = \t%s\nroots = \t%s\nf(roots) = \t%s\nnum. of calls = %s' % (x0,
                              rosenbrock(x0),
                              roots,
                              rosenbrock(roots),
                              global_ncalls.ncalls))
print('-------------------------------------------------')

rootsA = newton(f=rosenbrock, x0=x0, dx=dx)
print('Result with non-quadratic method (Part A):')
print('-------------------------------------------------')
print(f'roots = \t{rootsA}')
print(f'f(roots) = \t{rosenbrock(rootsA)}')
print(f'\nnum. of calls = {global_ncalls.ncalls}')
print('-------------------------------------------------')
print('\n')


# Test Newtons method on the Himmelblau function
# Initialise
x0 = np.array([10, 3], dtype='float64')
dx = np.array([1e-07, 1e-07], dtype='float64')

# Run test and compare
roots = newton_quad(f=himmelblau, x0=x0, dx=dx)
print('\nSolve the Himmelblau function ...\n')
print('Result:')
print('-------------------------------------------------')
print('x0 = \t\t%s\nf(x0) = \t%s\nroots = \t%s\nf(roots) = \t%s\nnum. of calls = %s' % (x0,
                              himmelblau(x0),
                              roots,
                              himmelblau(roots),
                              global_ncalls.ncalls))
print('-------------------------------------------------')

rootsA = newton(f=himmelblau, x0=x0, dx=dx)
print('Result with non-quadratic method (Part A):')
print('-------------------------------------------------')
print(f'roots = \t{rootsA}')
print(f'f(roots) = \t{himmelblau(rootsA)}')
print(f'\nnum. of calls = {global_ncalls.ncalls}')
print('-------------------------------------------------')
print('\n')


# Test Newtons method on some interesting example


