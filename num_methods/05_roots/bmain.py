import sys, time
import numpy as np
from roots import newton2
import global_ncalls
from systems import system_A, rosenbrock, himmelblau
from systems import jacobian_A, jacobian_ros, jacobian_him
# test of time.time()

#start=time.time()
#print('testing')
#stop=time.time()
#print(stop-start)

print("\nRun test: Newton's method with analytical jacobian ...")

# Test Newtons method with analytical jacobian  on A*x*y = 1
# Initialise
x0 = np.array([2, 1], dtype='float64')
dx = np.array([1e-08, 1e-08], dtype='float64')

# Run test
roots = newton2(f=system_A, jacobian=jacobian_A, x0=x0)
print('\nSolve A*x*y = 1, where exp(-x)+exp(-y)=1 + 1/A and A=10000 ...\n')
print('Result:')
print('-------------------------------------------------')
print('x0 = \t\t%s\nf(x0) = \t%s\nroots = \t%s\nf(roots) = \t%s\nnum. of calls = %s' % (x0,
                              system_A(x0),
                              roots,
                              system_A(roots),
                              global_ncalls.ncalls))
print('-------------------------------------------------')
print('\n')


# Test Newtons method on Rosenbrocks valley function
# Initialise
x0 = np.array([1, 10], dtype='float64')
dx = np.array([1e-07, 1e-07], dtype='float64')

# Run test
roots = newton2(f=rosenbrock, jacobian=jacobian_ros, x0=x0)
print('\nSolve Rosenbrocks valley function ...\n')
print('Result:')
print('-------------------------------------------------')
print('x0 = \t\t%s\nf(x0) = \t%s\nroots = \t%s\nf(roots) = \t%s\nnum. of calls = %s' % (x0,
                              rosenbrock(x0),
                              roots,
                              rosenbrock(roots),
                              global_ncalls.ncalls))
print('-------------------------------------------------')
print('\n')


# Test Newtons method on the Himmelblau function
# Initialise
x0 = np.array([10, 3], dtype='float64')
dx = np.array([1e-07, 1e-07], dtype='float64')

# Run test
roots = newton2(f=himmelblau, jacobian=jacobian_him, x0=x0)
print('\nSolve the Himmelblau function ...\n')
print('Result:')
print('-------------------------------------------------')
print('x0 = \t\t%s\nf(x0) = \t%s\nroots = \t%s\nf(roots) = \t%s\nnum. of calls = %s' % (x0,
                              himmelblau(x0),
                              roots,
                              himmelblau(roots),
                              global_ncalls.ncalls))
print('-------------------------------------------------')
print('\n')


# Test Newtons method on some interesting function
