import numpy as np
import global_ncalls


def system_A(p):
    """
    System: 
    A*x*y = 1
    where:
    exp(-x) + exp(-y) = 1 + 1/A
    And:
    A = 10000
    """
    global_ncalls.ncalls += 1
    # Initialise:
    x = p[0]
    y = p[1]
    A = 10000
    z = np.zeros(2, dtype='float64')
    # Calculate z
    z[0] = A*x*y - 1
    z[1] = np.exp(-x) + np.exp(-y) - 1 - (1/A)
 
    return z

def jacobian_A(p):
    """
    Jacobian of the system_A() function
    """
    global_ncalls.ncalls += 1
    # Initialise
    x = p[0]
    y = p[1]
    A = 10000
    J = np.zeros((2, 2), dtype='float64')
    # Fill J with first derivatives
    J[0,0] = A*y
    J[0,1] = A*x
    J[1,0] = - np.exp(-x)
    J[1,1] = - np.exp(-y)
    return J


def rosenbrock(p):
    """
    System:
    The gradient Rosenbrock's valley function 
    """
    # Initialise
    global_ncalls.ncalls += 1
    x = p[0]
    y = p[1]
    z = np.zeros(2, dtype='float64')
    # Calculate z
    z[0] = 2*(x - 1) + 400*(x*x - y)*x
    z[1] = 200*(y - x*x)
    return z

def jacobian_ros(p):
    """
    System:
    Jacobian of (the gradient of) Rosenbrock's valley function
    """
    # Initialise
    global_ncalls.ncalls += 1
    x = p[0]
    y = p[1]
    J = np.zeros((2, 2), dtype='float64')
    # Fill J with first derivatives
    J[0,0] = 2 + 800*x*x - 400*(y - x*x)
    J[1,0] = - 400*x
    J[0,1] = - 400*x
    J[1,1] = 200
    return J


def himmelblau(p):
    """
    System:
    The gradient of the Himmelblau function
    """
    global_ncalls.ncalls += 1
    # Initialise
    x = p[0]
    y = p[1]
    z = np.zeros(2, dtype='float64')
    # Calculate z
    z[0] = 4*(x*x + y - 11)*x + 2*(x + y*y -7)
    z[1] = 2*(x*x + y - 11) + 4*(x + y*y -7)*y
    return z

def jacobian_him(p):
    """
    System:
    Jacobian of (the gradient of)the Himmelblau function
    """
    # Initialise
    global_ncalls.ncalls += 1
    x = p[0]
    y = p[1]
    J = np.zeros((2,2), dtype='float64')
    # Calculate J
    J[0,0] = 12*x*x + 4*y - 42
    J[0,1] = 4*x + 4*y
    J[1,0] = 4*x + 4*y
    J[1,1] = 4*x + 12*y*y - 26
    return J

def hydrogen(p):
    """
    System:
    The Schrodinger equation for hydrogen
    """
    global_ncalls.ncalls += 1
    # Initialise
    x = p[0]
    y = p[1]
    z = np.zeros(2, dtype='float64')
    # calculate z
    z[0] = y
    z[1] = 2*(-1/8-(-1/2)) * x # rmax = 8 e=-1/2
    return z
