import numpy as np
import global_ncalls
from ode import rkdriver


def F(x, y):
    """
    'Right hand side'
    ODE where y'' = -y 
    """
    global_ncalls.ncalls += 1
    return np.array([y[1], -y[0]])

# Initialise
a = 0
b = np.pi**2
eps = 1e-03
acc = 1e-03
hstart = 0.1
y = np.array([0, 1])
steppers = ['rkstep12', 'rkstep23']

# Run driver
stepper = steppers[1]
print(f'# Run test: Rutta-Kutta driver with {stepper} on trigonometric ODE ...')
print('#')
print('#')
xlist, ylist, _ = rkdriver(F=F, 
                        ylist=y, 
                        a=a, 
                        b=b, 
                        method=stepper,
                        step=hstart, 
                        eps=eps, 
                        acc=acc)

# Print result:
print('# Result:')
print('# --------------------------------------------------------------------')
print('# \tx\t\t\ty[0]\t\t\ty[1]')
print('# --------------------------------------------------------------------')

for i in range(xlist.size):
    print(f'{xlist[i]}\t {ylist[i, 0]}\t {ylist[i, 1]}')

