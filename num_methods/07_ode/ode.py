import numpy as np
import global_ncalls

def rkstep12(F, x0, y0, h):
    """
    MIDPOINT METHOD

    Runge-Kutta stepper of order X=12 (1, 2).
    Parameters:
        F: 'Right hand side'
        x0: Point/position
        y0: Value of function
        h: Step-size
    """
    # Butchers tableu
    c1 = 0.
    c2 = 1.

    # Runga-Kutta matrix
    a21 = 1/2.

    # Weights
    b1 = 0.
    b2 = 1.
    bs1 = 1.
    bs2 = 0.

    # Calculate ks
    k1 = h * F(x0 + c1*h, y0)
    k2 = h * F(x0 + c2*h, y0 + a21*k1)

    # Next step and error
    yh = y0 + b1*k1 +b2*k2
    yhs = y0 + bs1*k1 + bs2*k2
    err = np.linalg.norm(yh - yhs)

    return yh, err

def rkstep23(F, x0, y0, h):
    """
    BOGACKI-SHAMPINE METHOD
    
    Runge-Kutta stepper of order X=23 (2, 3).
    Parameters:
        F: 'Right hand side'
        x0: Point/position
        y0: Value of function
        h: Step-size
    RK-steps:
        k0: Euler method
        k12: Midpoint method 
    """
    # Butchers tableu
    c1 = 0.
    c2 = 1./2.
    c3 = 3./4.
    c4 = 1.

    # Runga-Kutta matric
    a21 = 1/2.
    a31 = 0.
    a32 = 3./4.
    a41 = 2./9.
    a42 = 1./3.
    a43 = 4./9.

    # Weights
    b1 = 2./9.
    b2 = 1./3.
    b3 = 4./9.
    b4 = 0.

    bs1 = 7./24.
    bs2 = 1./4.
    bs3 = 1./3.
    bs4 = 1./8.

    # Calculate ks
    k1 = h * F(x0 + c1*h, y0)
    k2 = h * F(x0 + c2*h, y0 + a21*k1)
    k3 = h * F(x0 + c3*h, y0 + a31*k1 + a32*k2)
    k4 = h * F(x0 + c4*h, y0 + a41*k1 + a42*k2 + a43*k3)

    # Next step and error
    yh = y0 + b1*k1 + b2*k2 + b3*k3 + b4*k4
    yhs = y0 + bs1*k1 + bs2*k2 + bs3*k3 + bs4*k4
    err = np.linalg.norm(yh - yhs)

    return yh, err


def rkdriver(F, ylist, a, b, method, step = 1e-02, eps = 1e-06, acc = 1e-06):
    """
    Driver that calls the rkstep12 function.
    The driver monitors the local errors and tolerances
    and adjusts the step-size.
    Parameters:
        F: 'Right hand side'
        x0: point/position
        y0: value of function 
        eps: Relativ precision
        acc: Absolute precision
    """
    # Set chosen stepper
    if method.lower() == 'rkstep12':
        stepper = rkstep12
    elif method.lower() == 'rkstep23':
        stepper = rkstep23
    else:
        print('Unknown stepper')
        return
    
    
    # Initialise
    global_ncalls.ncalls = 0
    xlist = np.array([a], dtype='float64')
    ylist = np.array([ylist], dtype='int')
    calls = np.array([global_ncalls.ncalls], dtype='float64')

    # Start driver
    while global_ncalls.ncalls < 999:
        x0 = xlist[-1]
        y0 = ylist[-1]

        # Stopping criteria
        if x0 >= b:
            break

        # It step goes beyond interval, go to edge
        if (x0 + step) > b:
            step = b - x0

        # Call to stepper
        yh, err = stepper(F=F, x0=x0, y0=y0, h=step)
        
        # Set tolerance
        ynorm = np.linalg.norm(yh)
        tol = (acc + ynorm*eps)*np.sqrt(step/(b - a))
        
        # Monitor tolerance. Accept step if err is less than tol
        if err < tol:
            global_ncalls.ncalls += 1
            xlist = np.append(xlist, x0 + step)
            ylist = np.vstack([ylist, yh])
            calls = np.append(calls, global_ncalls.ncalls)

        # If no error, double the step. Otherwise decrease it.
        if err == 0:
            step *= 2
        else:
            step *= (tol/err)**(0.25)*0.95
    
    return xlist, ylist, calls
