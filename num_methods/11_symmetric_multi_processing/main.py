import sys
sys.path.append('../09_monte_carlo_integration')
import abmain as montecarlo
import multiprocessing as multi
import time

  
# MAIN
t0 = time.time()
print('As can be seen from the b.out from the Monte Carlo assignment, the total time used was around 57 seconds')
print('The problem is now run two times with multiprocessing ... ')

# Run with multi processing
p1 = multi.Process(target=montecarlo.bmain)
p2 = multi.Process(target=montecarlo.bmain)

p1.start()
p2.start()

p1.join()
p2.join()

print('------------------------------------------------------------------')
print(f'\nThe total time used with multiprocessing is {time.time()-t0:.3}')

