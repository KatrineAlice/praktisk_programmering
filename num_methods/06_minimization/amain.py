import sys, time
import numpy as np
from qnewton import minimise
import global_ncalls
from systems import rosenbrock, grad_ros, hessian_ros, himmelblau, grad_him, hessian_him
# test of time.time()

#start=time.time()
#print('testing')
#stop=time.time()
#print(stop-start)


# Test Newtons method on Rosenbrocks valley function
print("\nRun test: Newton's minimisation with backtracking linesearch ... ")
# Initialise
global_ncalls.ncalls = 0
x0 = np.array([-2, -2], dtype='float64')

# Run test
minimum = minimise(f=rosenbrock,
                   gradient=grad_ros,
                   hessian=hessian_ros, 
                   x0=x0)
# Print result
print("\nFind minimum of Rosenbrock's valley function ...\n")
print('Result:')
print('--------------------------------------------------')
print(f'x0 =\t\t {x0}')
print(f'f(x0) =\t\t {rosenbrock(x0)}')
print(f'minimum =\t {minimum}')
print(f'f(minimum) =\t {rosenbrock(minimum):.1f}') 
print(f'num. of calls =\t {global_ncalls.ncalls}')
print('--------------------------------------------------')



# Test Newtons method on the Himmelblau function
# Initialise
global_ncalls.ncalls = 0
x0 = np.array([2, 2], dtype='float64')

# Run test
minimum = minimise(f=himmelblau, 
                   gradient=grad_him, 
                   hessian=hessian_him, 
                   x0=x0)
print('\nFind minimum of the Himmelblau function ...\n')
print('Result:')
print('--------------------------------------------------')
print(f'x0 =\t\t {x0}')
print(f'f(x0) =\t\t {himmelblau(x0)}')
print(f'minimum =\t {minimum}')
print(f'f(minimum) =\t {himmelblau(minimum):.1f}') 
print(f'num. of calls =\t {global_ncalls.ncalls}')
print('--------------------------------------------------')

# Test Newtons method on some interesting function
