import numpy as np
from systems import rosenbrock, himmelblau, decay, master
from systems import grad_ros, grad_him, grad_master
from systems import hessian_ros, hessian_him
from qnewton import sr1, minimise
import global_ncalls
import sys
sys.path.append('../05_roots')
import roots

print('\nRun test: Quasi-Newton minimisation with user provided gradient and hessian ...\n')

################## ROSENBROCK #####################
# Initialise
global_ncalls.ncalls = 0
x0 = np.array([-2, 2], dtype='float64')

# Test
minimum = sr1(f=rosenbrock,gradient=grad_ros,x0=x0)

print("\nFind minimum of Rosenbrock's valley function ...\n")
# Print result
print('Result:')
print('--------------------------------------------------')
print(f'x0 =\t\t {x0}')
print(f'f(x0) =\t\t {rosenbrock(x0)}')
print(f'minimum =\t {minimum}')
print(f'f(minimum) =\t {rosenbrock(minimum):.1f}') 
print(f'num. of calls =\t {global_ncalls.ncalls}')
print('--------------------------------------------------')
print('Comparison of steps to other methods ...')
print('--------------------------------------------------')
print('Newton minimisation from (A) ...')
minimum = minimise(f=rosenbrock,gradient=grad_ros,hessian=hessian_ros,x0=x0)
print(f'num. of calls =\t {global_ncalls.ncalls}')

print('Root finding ...')
dx = np.array([1e-08, 1e-08], dtype='float64')
root = roots.newton(f=grad_ros,x0=x0,dx=dx)
print(f'num. of calls =\t {global_ncalls.ncalls}')
print('--------------------------------------------------\n')


################# HIMMELBLAU ######################

# Initialise
global_ncalls.ncalls = 0
x0 = np.array([2, 2], dtype='float64')

# Test
print('\nFind minimum of the Himmelblau function ...\n')
minimum = sr1(f=himmelblau,gradient=grad_him,x0=x0)

# Print result
print('Result:')
print('--------------------------------------------------')
print(f'x0 =\t\t {x0}')
print(f'f(x0) =\t\t {himmelblau(x0)}')
print(f'minimum =\t {minimum}')
print(f'f(minimum) =\t {himmelblau(minimum):.1f}') 
print(f'num. of calls =\t {global_ncalls.ncalls}')
print('--------------------------------------------------')
print('Comparison of steps to other methods ...')
print('--------------------------------------------------')
print('Newton minimisation from (A) ...')
minimum = minimise(f=himmelblau,gradient=grad_him,hessian=hessian_him,x0=x0)
print(f'num. of calls =\t {global_ncalls.ncalls}')

print('Root finding ...')
dx = np.array([1e-08, 1e-08], dtype='float64')
root = roots.newton(f=grad_him,x0=x0,dx=dx)
print(f'num. of calls =\t {global_ncalls.ncalls}')
print('--------------------------------------------------\n')



############### TEST ON THE MASTER FUNCTION #################

print('\nRun test: Use implementation to optimise ...\n')
# Initialise
t, y, s = np.loadtxt('decay_data.txt', unpack=True)
x0 = np.array([1, 1, 1], dtype='float64')
xs = np.linspace(0,10,50)

# Create anonymous functions with p as variable
function = lambda p: master(t, y, s, p)
gradient = lambda p: grad_master(t, y, s, p)

# Optimise by the implementation
parameters = sr1(f=function,gradient=gradient,x0=x0)

# Print result
print('Find minimum of the master function ...\n')
print('Result:')
print('--------------------------------------------------')
print('\txs\t\tdecay(xs,parameters)')
for i in range(xs.size):
    print(f'{xs[i]}\t{decay(xs[i], parameters):}')
print('--------------------------------------------------')

# Save data in separate file for further use
print("\nSaving optimised decay data to file: 'optimised.data' ...\n")
out = np.array([xs, decay(xs, parameters)])
np.savetxt('optimised.data',out.T)
print('Done.')
