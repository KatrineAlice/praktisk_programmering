import numpy as np
import global_ncalls

def rosenbrock(p):
    """
    System:
    Rosenbrock's valley function
    """
    # Initialise
    x = p[0]
    y = p[1]
    
    # Calculate Rosenbrock's valley function
    ros = (1 - x)**2 + 100*(y - x*x)**2
    
    return ros

def grad_ros(p):
    """
    System:
    The gradient Rosenbrock's valley function 
    """
    # Initialise
    x = p[0]
    y = p[1]
    grad = np.zeros(2, dtype='float64')
    
    # Calculate z
    grad[0] = 2*(x - 1) + 400*(x*x - y)*x
    grad[1] = 200*(y - x*x)
    
    return grad

def hessian_ros(p):
    """
    System:
    Hessian of Rosenbrock's valley function
    """
    # Initialise
    x = p[0]
    y = p[1]
    H = np.zeros((2, 2), dtype='float64')
    
    # Fill H with second derivatives
    H[0,0] = 2 + 800*x*x - 400*(y - x*x)
    H[1,0] = - 400*x
    H[0,1] = - 400*x
    H[1,1] = 200
    
    return H

def himmelblau(p):
    """
    System:
    The himmelblau function
    """
    # Initialise
    x = p[0]
    y = p[1]

    # Calculate the himmelblau function
    him = (x*x + y - 11)**2 + (x + y*y - 7)**2
    
    return him

def grad_him(p):
    """
    System:
    The gradient of the Himmelblau function
    """
    # Initialise
    x = p[0]
    y = p[1]
    grad = np.zeros(2, dtype='float64')
    
    # Calculate the gradient
    grad[0] = 4*(x*x + y - 11)*x + 2*(x + y*y -7)
    grad[1] = 2*(x*x + y - 11) + 4*(x + y*y -7)*y
    
    return grad

def hessian_him(p):
    """
    System:
    Hessian of the Himmelblau function
    """
    # Initialise
    x = p[0]
    y = p[1]
    H = np.zeros((2,2), dtype='float64')
    
    # Calculate J
    H[0,0] = 12*x*x + 4*y - 42
    H[0,1] = 4*x + 4*y
    H[1,0] = 4*x + 4*y
    H[1,1] = 4*x + 12*y*y - 26
    
    return H

def decay(t, p):
    """
    System:
    The given fit-function for the decay system.
    Note that p now has 3 entrances
    Parameters:
        A: Initial amount
        T: Lifetime
        B: Background noise
        t: time
    """
    # Initialise
    A = p[0]
    T = p[1]
    B = p[2]
    
    # Calculate f
    f = A*np.exp(-t/T)+B
    
    return f

def grad_dec(t, p):
    """
    System:
    The gradient of the decay fit-function.
    The same parameters as the decay function.
    """
    # Initialise
    A = p[0]
    T = p[1]
    B = p[2]
    g = np.zeros(3, dtype='float64')
    
    # Calculate the gradient
    g[0] = np.exp(-t/T)
    g[1] = (A*t*np.exp(-t/T)) / (T*T)
    g[2] = 1
    
    return g

def master(t, y, s, p):
    """
    System:
    Master function of the squared loss.
    Used to determine the lifetime by minimisation.
    Parameters:
        y: Activity
        s: Uncertainty (sigma)
        p: The parameters from the decay
    """
    # Initialise
    n = t.size
    master_sum = 0
    
    # Calculate the sum (master function)
    for i in range(n):
        master_sum += ((decay(t[i], p) - y[i])**2) / (s[i]*s[i])
    
    return master_sum

def grad_master(t, y, s, p):
    """
    System:
    The gradient of the master function.
    The parameters are as in the master function itself.
    """
    # Initialise
    n = t.size
    grad = np.zeros(3, dtype='float64')

    # Calculate the gradient
    for i in range(n):
        dec = decay(t[i], p)
        gd = grad_dec(t[i], p)
        for j in range(3):
            grad[j] += gd[j]*(2*(dec - y[i]) / (s[i]*s[i]))

    return grad
