import sys
import global_ncalls
import numpy as np
sys.path.append('../02_linear_equations')
from lin_solver import gv_decomp
from lin_solver import gv_solver


def minimise(f, gradient, hessian, x0, alpha = 1e-4, eps = 1e-10):
    """
    This routine finds the minimum of a function using Newton's method.
    The derivatives (gradiant and hessian) are supplied by the user.
    It uses Given's rotation for QR_decomposition.
    """
    # Initialise
    global_ncalls.ncalls = 0
    x = np.copy(x0)
    n = x.size
    fx = f(x)
    df = gradient(x)
    stepmax = 5000

    # Begin root search
    while True:
        global_ncalls.ncalls += 1

        # Calculate Hessian matrix
        H = hessian(x)

        # Decompose and solve using Given's rotations
        H = gv_decomp(H)
        Dx = -df
        Dx = gv_solver(H, Dx)

        # Begin linear backtracking linesearch
        lambd = 2.0
        dfDx = np.dot(df, Dx)

        while True: 
            lambd /= 2
            s = Dx*lambd
            y = x + s
            fy = f(y)

            # Check Armijo condition
            if (fy < fx + alpha*lambd*dfDx) or (lambd < (1/128.)):
                break

        # Save the last approximation
        x = y
        fx = fy
        df = gradient(x)

        dfnorm = np.linalg.norm(df)
        if (dfnorm < eps) or (global_ncalls.ncalls > stepmax):
            break

    if global_ncalls.ncalls > stepmax:
        print('\nToo many steps used!')
        exit()
    else:
        return x



def sr1(f, gradient, x0, alpha = 1e-04, eps = 1e-10):
    # Initialise
    global_ncalls.ncalls = 0
    x = np.copy(x0)
    n = x.size
    fx = f(x)
    dfdx = gradient(x)
    stepmax = 5000

    # To save inverse of Hessian, note H^{-1} is denoted B
    B = np.identity(n, dtype='float64')

    # Minimization
    while True:
        global_ncalls.ncalls += 1

        # Derivatives into the inverse Hessian
        Dx = np.dot(B, -dfdx)
        
        # Begin linesearch
        lambd = 2.
        dfDx = np.dot(dfdx, Dx)

        while True:
            lambd /= 2
            y = x + Dx*lambd
            fy = f(y)

            # Check Armijo condition
            if (abs(fy) < abs(fx) + alpha*lambd*dfDx):
                break
            # Restart if the update diverges
            if (lambd < (1/128.)):
                B = np.identity(n, dtype='float64')
                break
        
        # Apply update
        dfdx_y = gradient(y)
        z = dfdx_y - dfdx
        u = Dx*lambd - np.dot(B, z)

        # SR1 update
        B += np.outer(u, u)/np.dot(u, z)
    
        ## Symmetric Broyden update:
        #s = Dx*lambd
        #gamma = np.dot(z, u) / np.dot(z, s) /2
        #a = (u - gamma*s) / np.dot(z, s)
        #B += np.outer(a, s) + np.outer(s, a)


        # Save the last approximation:
        x = y
        fx = fy
        dfdx = gradient(x)

        dfdxnorm = np.linalg.norm(dfdx)
        Dxnorm = np.linalg.norm(Dx)

        if (dfdxnorm < eps) or (global_ncalls.ncalls > stepmax):
            break

    if global_ncalls.ncalls > stepmax:
        print('\nToo many steps used!')
        exit()
    else:
        return x

