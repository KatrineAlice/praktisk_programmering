import sys
import numpy as np
from jacobi import eigenvalue

#### PART B ####
"""
Test of jacobi diagonalisation using the eigenvalue-by-eigenvalue method
"""
# Allow for arguments from Makefile for timing
if len(sys.argv)>1:
    n = int(sys.argv[1])
else:
    n = 3

# Make sure that the same random numbers are used each time
np.random.seed(20)

# Initialise A as quadratic and symmetric
a = np.random.rand(n,n)
A = a + a.T
A_check = np.copy(A)

# Test of the jacobi diagonalisation
print('Run test: Jacobi diagonalisation, eigenvalue-by-eigenvalue method ...')
sweeps, e, V = eigenvalue(A)
print('\nThe matrix used:\n A = \n', A_check)
print('\nThe number of rotations (sweeps) used:', sweeps)
print('\nA has eigenvalues:\n e = \n', e)
print('\nA has eigenvectors:\n V = \n', V)
print('\nAfter the diagonalisation,\n A = \n', A)

# Test that V.T * A * V = eigenvalues
print('\nRun test: V.T*A*V == e: ...')
e_check = np.dot(np.dot(V.T, A_check), V)
# to make check easier, e is sorted and flattened
e_check = np.sort(np.ravel((np.diagonal(e_check))))
e = np.sort(np.ravel((e)))
print(np.allclose(e_check,e))

# Test that the eigenvalues match np.linealg:
print('\nRun test: The eigenvalues are identical to the ones found by numpy.linalg: ...')
w, v = np.linalg.eig(A_check)
w = np.sort(np.ravel(w))
print(np.allclose(w, e))
print('\n')
