import numpy as np

"""
Matrix diagonalisation by jacobi eigenvalue method. 
The method can be either cyclic or eigenvalue-by-eigenvalue.
Both methods use the same rotation algorithm.
"""

def cyclic(A):
    """
    Matrix diagonalisation of a real and symmetric matrix A
    by the cyclic Jacobi eigenvalue method
    """
    # Assert that A is quadratic, symmetric and real
    n, m = A.shape
    assert n == m
    assert np.allclose(A.T, A)
    assert np.allclose(A, A.real)

    # Initialise
    e = np.zeros((n,1))
    V = np.identity(n)
    sweeps = 0
    changed = True

    # e stores all diag. elements
    for i in range(n):
        e[i] = A[i,i]

    while changed:
        sweeps += 1
        changed = False
        for p in range(n):
            for q in range(p + 1, n):
                changed, sweeps = rotation(A, e, V, 
                                           p, q, n, 
                                           changed, sweeps, False)

    return sweeps, e, V

def eigenvalue(A):
    """
    Matrix diagonalisation of a real and symmetric matrix A, 
    by the classic Jacobi eigenvalue-by-eigenvalue method.
    """
     # Assert that A is quadratic, symmetric and real
    n, m = A.shape
    assert n == m
    assert np.allclose(A.T, A)
    assert np.allclose(A, A.real)

    # Initialise
    e = np.zeros((n,1))
    V = np.identity(n)
    sweeps = 0

    # e stores all diag. elements
    for i in range(n):
        e[i] = A[i,i]

    for p in range(n - 1):
        changed = True
        while changed:
            changed = False
            for q in range(p+1,n):
                changed, sweeps = rotation(A,e,V,
                                           p,q,n,
                                           changed,sweeps,True)

    return sweeps, e, V


def rotation(A, e, V, p, q, n, changed, sweeps, pre_rows):
    """
    This routine performs the Jacobi rotation.
    """
    # Get the entries
    app = e[p]
    aqq = e[q]
    apq = A[p,q]

    # Calculate the eigenvalues
    #phi = 0.5*np.arctan2(2*apq, aqq-app) #smallest
    phi = 0.5 * (np.pi + np.arctan2(2*apq, aqq-app)) #largest
    c = np.cos(phi)
    s = np.sin(phi)

    # New diagnoal elements
    app1 = c*c*app - 2*s*c*apq + s*s*aqq
    aqq1 = s*s*app + 2*s*c*apq + c*c*aqq

    # Check that they match
    if app1 != app or aqq1 != aqq:
        changed = True
        sweeps += 1

        # Update the diagonal element and apq
        e[p] = app1
        e[q] = aqq1
        A[p,q] = 0

        # Update the remaining elements
        if not pre_rows:
            for i in range(p):
                aip = A[i,p]
                aiq = A[i,q]
                A[i,p] = c*aip - s*aiq
                A[i,q] = c*aiq + s*aip

        for i in range(p+1,q):
            api = A[p,i]
            aiq = A[i,q]
            A[p,i] = c*api - s*aiq
            A[i,q] = c*aiq + s*api

        for i in range(q+1,n):
            api = A[p,i]
            aqi = A[q,i]
            A[p,i] = c*api - s*aqi
            A[q,i] = c*aqi + s*api

        for i in range (n):
            vip = V[i,p]
            viq = V[i,q]
            V[i,p] = c*vip - s*viq
            V[i,q] = c*viq + s*vip

    return changed, sweeps
