import numpy as np
import os
import sys
sys.path.append('../02_linear_equations')
import lin_solver
sys.path.append('../03_eigenvalues')
import jacobi

def evaluate(c, funcs, x):
    """
    Evaluates the fit of a linear combination:
    \sum{c_i * f_i(x)} at x where f_i is a list of functions
    """
    return sum([c[i]*funcs[i](x) for i in range(len(funcs))])

def qr_lsfit(funcs, x, y, dy):
    """
    Least mean square and QR-decomposition by Givens rotations:

    Calculates the fit of a linear combination:
    \sum{c_i * f_i(x)}, where f_i is a list of functions, to
    the data (x, y) with error dy.
    """
    # Inistialise
    n = len(x)
    m = len(funcs)
    A = np.zeros((n, m), dtype='float64')
    b = np.zeros(n, dtype='float64')
    c = np.zeros(m, dtype='float64')
    dc = np.zeros(m, dtype='float64')

    # Construct A and c
    for i in range(n):
        b[i] = y[i]/dy[i]
        for k in range(m):
            A[i,k] = funcs[k](x[i])/dy[i]
    
    # decompose using givens rotations with in-build back-sub.
    Q = lin_solver.gv_decomp(A=A)
    x = lin_solver.gv_solver(A=Q, b=b)

    # save the solution in c
    for i in range(m):
        c[i] = x[i]

    # calculate the inverse
    B = lin_solver.gv_inverse(A=lin_solver.make_R(Q))

    # calculate the covariance matrix S
    S = np.dot(B, B.T)

    # calculate uncertainties of coefficients from S
    for i in range(m):
        dc[i] = np.sqrt(S[i, i])

    return c, dc

def singular_decomp(A):
    """
    Singular value decomposition using Jacobi's algorithm
    for diagonalisation.
    The matrix A is decomposed into A = U S V.T
    U = A V D^{1/2} and S = D^{1/2}. 
    D is diagonal and contains the eigenvalues for A
    V contains the eigenvectors for A
    """
    # Initialise
    n, m = A.shape
    U = np.zeros((n,m),dtype='float64')

    # Cyclic Jacobi diag. of A.T A
    _, e, V = jacobi.cyclic(np.dot(A.T, A))

    # Calculate U
    U = np.dot(A, V)
    for i in range(m):
        e[i] = np.sqrt(e[i])
        U[:, i] /= e[i]

    return U, e, V

def singular_solve(U, e, V, b):
    """
    Computes the least-square solution to (U S V.T)*x = b,
    by singular value decomposition
    """
    # Calculate S V.T x = U.T b and solve for x
    y = np.dot(U.T, b)

    for i in range(len(y)):
        y[i] /= e[i]

    x = np.dot(V, y)

    return x

def singular_lsfit(funcs, x, y, dy):
    """
    As qr_lsfit() but with singular value decomposition
    """
    # Initialise
    n = len(x)
    m = len(funcs)
    A = np.zeros((n, m), dtype='float64')
    b = np.zeros(n, dtype='float64')
    c = np.zeros(m, dtype='float64')
    dc = np.zeros(m, dtype='float64')

    # Construct A and c
    for i in range(n):
        b[i] = y[i] / dy[i]
        for k in range(m):
            A[i, k] = funcs[k](x[i]) / dy[i]

    # Decompose and solve
    U, e, V = singular_decomp(A)
    c = singular_solve(U, e, V, b)

    # Covariance matrix S
    VDinv = np.zeros((m, m), dtype='float64')
    for i in range(m):
        dinv_i = 1/(e[i]*e[i])
        for j in range(m):
            VDinv[j, i] = V[j, i]*dinv_i
    S = np.dot(VDinv, V.T)

    # Uncertainties on the coefficients from S
    for i in range(m):
        dc[i] = np.sqrt(S[i, i])

    return c, dc
