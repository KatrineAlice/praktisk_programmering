import numpy as np
from least_squares import qr_lsfit 
from least_squares import evaluate 

#### PART A AND B ####
'''
Test of the least-squares fit with QR-decomp.
'''

# Define functions for flist
def inv(x):
    return 1/x

def const(x):
    return 1

def lin(x):
    return x

# Load data and fit
x, y, dy = np.loadtxt('data.txt')
fitfunctions = [inv, const, lin]

c, dc = qr_lsfit(funcs=fitfunctions, x=x, y=y, dy=dy)

# Print for plot of data, fit and uncertainties
xs = 150
xfit = np.linspace(x[0], x[-1], xs)

for i in range(len(x)):
    print('%s\t%s\t%s' % (x[i], y[i], dy[i]))
print('\n')
for i in range(xs):
    yfit = evaluate(c, fitfunctions, xfit[i])
    yfit_u = evaluate(c + dc, fitfunctions, xfit[i])
    yfit_l = evaluate(c - dc, fitfunctions, xfit[i])
    print('%s\t%s\t%s\t%s' % (xfit[i], yfit, yfit_u, yfit_l))
