import numpy as np
import neural_network as nw


def gaussian_wavelet(x):
    """
    Activation function to train the ANN
    """
    return x*np.exp(-x*x)

def wavelet(x):
    """
    Function to interpolate
    """
    return np.cos(5*x)*np.exp(-x*x)

# MAIN PART A
print('# Testing 1D neural network ... \n#')
print('# Using gaussian wavelet to train ann ... \n#')

# Initilise 
num_neurons = 20
ann = nw.ANN_1D(num_neurons, gaussian_wavelet)
x = np.linspace(-3, 3, 100)
gx = wavelet(x)

# Train ann and feed forward
print('# Interpolate wavelet function... \n#')
ann.train(x, gx)
print('# Feed forward ... \n#')
y = ann.feed_forward(x)
print('# Done.')

# Print output
print('# ---------------------------------------')
print('# Result:')
print('# ---------------------------------------')
print('# x\ty\tgx')
[print(f'{x[i]:.16}\t{y[i]:.16}\t{gx[i]:.16}') for i in range(len(x))]
