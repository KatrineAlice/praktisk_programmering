import numpy as np
import neural_network as nw


def gaussian_wavelet(x, y):
    """
    Activation function to train the ANN
    """
    return y*x*np.exp(-x*x)*np.exp(-y*y)

def wavelet(x, y):
    """
    Function to interpolate
    """
    return np.cos(5*x)*np.exp(-x*x)*np.exp(-y*y)

# MAIN PART A
print('# Testing 1D neural network ... \n#')
print('# Using gaussian wavelet to train ann ... \n#')

# Initilise 
num_neurons = 20
ann = nw.ANN_2D(num_neurons, gaussian_wavelet)
x = np.linspace(-1.5, 1.5, 100)
y = np.linspace(-2.5, 2.5, 100)
gx = wavelet(x, y)

# Train ann and feed forward
print('# Interpolate wavelet function... \n#')
ann.train([x, y], gx)
print('# Feed forward ... \n#')
z = ann.feed_forward([x, y])
print('# Done.')

# Print output
print('# ---------------------------------------')
print('# Result:')
print('# ---------------------------------------')
print('# x\ty\tz\tgx')
[print(f'{x[i]:.16}\t{y[i]:.16}\t{z[i]:.16}\t{gx[i]:.16}') for i in range(len(x))]
