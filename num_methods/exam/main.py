import numpy as np
import global_ncalls
from ode import rkdriver

def F(x, y):
    """
    'Right hand side'
    ODE where y'' = -y 
    """
    global_ncalls.ncalls += 1
    return np.array([y[1], -y[0]])

def run_driver(F, y, a, b, stepper1, stepper2, hstart, eps, acc):
    # Reset call counter
    global_ncalls.ncalls = 0

    # Run driver (New lines are marked with # instead of \n, so the data can be easily loaded if needed)
    print(f'# Run test: Rutta-Kutta driver with {stepper1} and {stepper2} on trigonometric ODE ...')
    print('#')
    print('#')
    xlist, ylist, calls = rkdriver(F=F, 
                        ylist=y, 
                        a=a, 
                        b=b, 
                        method1=stepper1,
                        method2=stepper2,
                        step=hstart, 
                        eps=eps, 
                        acc=acc)

    # Print result:
    print('# Result:')
    print('# -----------------------------------------------------------------------------')
    print('# \t x \t\t\t y[0] \t\t\t y[1] \t\t calls')
    print('# -----------------------------------------------------------------------------')

    for i in range(xlist.size):
        print(f'{xlist[i]}\t {ylist[i, 0]}\t {ylist[i, 1]}\t{int(calls[i])}')

# MAIN
"""
In part B the driver should be modified to store the values. 
This I already did for part a so instead I:
    - added a list to store the number of calls
    - added rkstep23 -- a stepper of order 2, 3
    - Made plots of both a.out and b.out
"""

# Initialise
a = 0
b = np.pi**2
eps = 1e-03
acc = 1e-03
hstart = 0.1
y = np.array([0, 1])
stepper = ['rkstep12', 'rkstep23', 'step2m']


# Runs 
# (for extra runs \n is added in between to create indices for each data set)
run_driver(F, y, a, b, stepper[0], stepper[2], hstart, eps, acc)
print('\n')
run_driver(F, y, a, b, stepper[1], stepper[2], hstart, eps, acc)
