import numpy as np

class interpolation:

    """ Class for linear interpolation. """ 

    def __init__(self, x, y):
        """
        initializes and empty object (self) and fills it with
        x and y (flattened to 1D if they are not already)
        """
        x = np.asarray(x)
        y = np.asarray(y)
        x = np.ravel(x)
        y = np.ravel(y)

        if x.shape != y.shape:
            raise ValueError("x and y must have the same length")
        self.x = x
        self.y = y

        self.n = int(len(self.x))

        self.qb, self.qc = self.qspline_params()
        self.cb, self.cc, self.cd = self.cspline_params()


    def binary_search(self, z):
        """
        Binary search algorithm.            
        
        See:
        https://en.wikipedia.org/wiki/Binary_search_algorithm
        and Table 2 Chapter 1 Interpolation. 
        """
        i = 0
        j = self.n-1
        while j-i > 1:
            m = int(np.floor((i+j)/2))
            if z > self.x[m]:
                i = m
            else:
                j = m
        # calculate slope
        dx = self.x[i+1]-self.x[i]
        dy = self.y[i+1] - self.y[i]
        slope = dy/dx

        return slope, i

# FOR PART A, LINEAR INTERPOLATION

    def linterp_integ(self, z):
    
        """ Linear interpolation integration """    
        
        result = 0
        slope, i = self.binary_search(z)
        for j in range(i):
            dx = self.x[j+1] - self.x[j]
            dy = self.y[j+1] - self.y[j]
            result += self.y[j]*dx + (1/2)*dy*dx**2
        result += (self.y[i]*(z - self.x[i]) 
                  + (1/2)*(self.y[i+1] - self.y[i])*(z - self.x[i])**2) 
        
        return result


    def linterp(self, z):
    
        """ Linear interpolation """

        z = np.asarray(z)
        s = np.zeros(z.shape)
        si = np.zeros(z.shape)
        for j in range(z.size):
            slope, i = self.binary_search(z[j])
            s[j] = self.y[i] + slope*(z[j] - self.x[i])
            si[j] = self.linterp_integ(z[j])
        
        return s, si

# FOR PART B, QUADRATIC SPLINE

    def qspline_params(self):
        """ 
        Calculate coefficients b and c 
        See Table 3, Chapter 1: Interpolation
        """
        b = np.zeros(self.n-1)
        c = np.zeros(self.n-1)
        dx = np.zeros(self.n-1)
        dy = np.zeros(self.n-1)
        slope = np.zeros(self.n-1)

        # calculate slope
        for j in range(self.n-1):
            dx[j] = self.x[j+1]-self.x[j]
            dy[j] = self.y[j+1]-self.y[j]
            slope[j] = dy[j]/dx[j]

        # recursion up
        for j in range(self.n-2):
            c[j+1] = (slope[j+1] - slope[j] - c[j]*dx[j])/dx[j+1]
    
        # recursion down
        c[-1] = c[-1]/2
        down = range(self.n-2)
        for j in down[::-1]:
            c[j] = (slope[j+1] - slope[j] - c[j+1]*dx[j+1])/dx[j]

        # that's c, now find b
        for j in range(self.n-1):
            b[j] = slope[j] - c[j]*dx[j]
        return b, c

    def qspline_integ(self, z):
        
        """ quadratic spline integration """
        
        result = 0
        slope, i = self.binary_search(z)
        b = self.qb
        c = self.qc
        for j in range(i):
            dx = self.x[j+1] - self.x[j]
            result += (self.y[j]*dx 
                       + (1/2)*b[j]*dx**2 
                       + (1/3)*c[j]*dx**2 )
        result += (self.y[i]*(z - self.x[i]) 
                  + (1/2)*b[i]*(z - self.x[i])**2
                  + (1/3)*c[i]*(z - self.x[i])**2) 
        return result

    def qspline(self, z, flag=1, dflag=1, iflag=1):

        """ quadratic spline interpolation"""

        z = np.asarray(z)
        s = np.zeros(z.shape)
        sd= np.zeros(z.shape)
        si= np.zeros(z.shape)
        b = self.qb
        c = self.qc

        for j in range(z.size):
            slope, i = self.binary_search(z[j])
            # function
            if flag is not None: 
                s[j] = (self.y[i] 
                        + b[i]*(z[j] - self.x[i]) 
                        + c[i]*(z[j] - self.x[i])**2)
            # derivative
            if dflag is not None:
                sd[j] = (b[i] 
                        + 2*c[i]*(z[j] - self.x[i]))
            # integral
            if iflag is not None:
                si[j] = self.qspline_integ(z[j])
        return s, sd, si

# FOR PART C, CUBIC SPLINE

    def cspline_params(self):
        """ 
        Calculate coefficients b, c and d 
        See Table 4, Chapter 1: Interpolation
        """
        b = np.zeros(self.n)
        c = np.zeros(self.n-1)
        d = np.zeros(self.n-1)
        dx = np.zeros(self.n-1)
        dy = np.zeros(self.n-1)
        slope = np.zeros(self.n-1)
        D = np.zeros(self.n)
        Q = np.zeros(self.n-1)
        B = np.zeros(self.n)
        
        # calculate slope
        for j in range(self.n-1):
            dx[j] = self.x[j+1]-self.x[j]
            dy[j] = self.y[j+1]-self.y[j]
            slope[j] = dy[j]/dx[j]

        # D
        D[0] = 2
        for j in range(self.n-2):
            D[j+1] = 2*dx[j]/dx[j+1] + 2
        D[-1] = 2

        # Q
        Q[0] = 1
        for j in range(self.n-2):
            Q[j+1] = dx[j]/dx[j+1]

        # B
        B[0] = 3*slope[0]
        for j in range(self.n-2):
            B[j+1] = 3*(slope[j] + slope[j+1]*dx[j]/dx[j+1])
        B[-1] = 3*slope[-2]
        
        # gauss elimination
        for j in range(1, self.n):
            D[j] -= Q[j-1]/D[j-1]
            B[j] -= B[j-1]/D[j-1]

        # Back substitution
        b[-1] = B[-1]/D[-1]
        down = range(self.n-2)
        for j in down[::-1]:
            b[j] = (B[j] - Q[j]*b[j+1])/D[j]
        
        # that's b, now calculate c and d
        for j in range(self.n-1):
            c[j] = (-2*b[j] - b[j+1] + 3*slope[j])/dx[j]
            d[j] = (b[j] + b[j+1] - 2*slope[j])/dx[j]/dx[j]

        return b, c, d

    def cspline_integ(self, z):
        
        """ cubic spline integration """
        
        result = 0
        slope, i = self.binary_search(z)
        b = self.cb
        c = self.cc
        d = self.cd
        for j in range(i):
            dx = self.x[j+1] - self.x[j]
            result += (self.y[j]*dx 
                       + (1/2)*b[j]*dx**2 
                       + (1/3)*c[j]*dx**2 
                       + (1/4)*d[j]*dx**3)
        result += (self.y[i]*(z - self.x[i]) 
                  + (1/2)*b[i]*(z - self.x[i])**2
                  + (1/3)*c[i]*(z - self.x[i])**2
                  + (1/4)*d[i]*(z - self.x[i])**3) 
        return result

    def cspline(self, z, flag=1, dflag=1, iflag=1):

        """ cubic spline interpolation"""

        z = np.asarray(z)
        s = np.zeros(z.shape)
        sd= np.zeros(z.shape)
        si= np.zeros(z.shape)
        b = self.cb
        c = self.cc
        d = self.cd

        for j in range(z.size):
            slope, i = self.binary_search(z[j])
            # function
            if flag is not None: 
                s[j] = (self.y[i] 
                        + b[i]*(z[j] - self.x[i]) 
                        + c[i]*(z[j] - self.x[i])**2
                        + d[i]*(z[j] - self.x[i])**3)
            # derivative
            if dflag is not None:
                sd[j] = (b[i] 
                        + 2*c[i]*(z[j] - self.x[i])
                        + 3*d[i]*(z[j] - self.x[i])**2)
            # integral
            if iflag is not None:
                si[j] = self.qspline_integ(z[j])
        return s, sd, si


