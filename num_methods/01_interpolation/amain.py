import numpy as np
from interpol import interpolation as interpol

""" 
Define start and end point and the number of points 
in the function (n) and in the interpolation (N)
"""
n = 50
N = 200
start = 0
end = 4 * np.pi

# Make input -- as an example, the cosine function is used.
x = np.linspace(start, end, n)
y = np.cos(x)
z = np.linspace(start, end, N)
y_int = np.sin(x)


# Initialize the class
intp_cos = interpol(x, y)

# Interpolate
s, si = intp_cos.linterp(z)

# Print output 
for i in range(0, N):
    if i < n:
        print('%s\t%s\t%s\t%s\t%s\t%s' % (z[i], s[i], si[i], x[i], y[i], y_int[i]))
    else:
        print('%s\t%s\t%s' % (z[i], s[i], si[i]))
