import numpy as np
from lin_solver import gs_decomp, gs_inverse

# To make sure the same seed of random numbers are used in each run:
np.random.seed(20)
rtol = 1e-20

##################
####  PART B  ####
##################

# Create random square matrices (n x n) A, R and vector b
n = 5

A_init = np.random.rand(n, n)
A = np.copy(A_init)
R = np.random.rand(n, n)

# Check of gram-schmidt solver
Q, R = gs_decomp(A=A_init, R=R)
B = gs_inverse(Q=Q, R=R)
AB = np.dot(A, B)

print('PART B: Check of gram-schmidt inverse:')
print('A has dimensions {}x{} and is given by\n{}'.format(*A.shape, A))
print('\nThe inverse after decomposition is B=\n{}'.format(B))
print('\nAB == I within an error of {}: {}'.format(rtol,np.allclose(AB, np.identity(n))))
