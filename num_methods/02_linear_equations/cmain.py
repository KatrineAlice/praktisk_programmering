import numpy as np
from lin_solver import gv_decomp, gv_solver, gv_inverse, gs_decomp, gs_solver, gs_inverse

# To make sure the same seed of random numbers are used in each run:
np.random.seed(20)
rtol = 1e-20

# Create a random square matrix (n x n) 
n = 5

A_init = np.random.rand(n, n)
A = np.copy(A_init)
A_gs = np.copy(A_init)
A_inv = np.copy(A_init)
R = np.random.rand(n, n)
b_init = np.random.rand(n)
b = np.copy(b_init)

##################
#### PART C.1 ####
##################

# Check of decomposition by Givens rotations
θ = gv_decomp(A=A_init)
Q, R = gs_decomp(A=A_gs, R=R)

print('PART C.1: Check of decomposition of A and substitution to θ:')
print('A has dimensions {}x{} and A =\n{}'.format(*A.shape,A))
print('\nFrom the decomposition: θ =\n{}'.format(θ))

##################
#### PART C.2 ####
##################

# Check of solve AFTER decomposition
x = gv_solver(θ, b_init)

print('\n\nPART C.2: Check of solve of A after decomposition')
print('The solution when using Givens-rotation is x =\n{}'.format(x))

# check that it matched gs-method:
x_gs = gs_solver(Q=Q, R=R, b=b)
print('\nThe solution when using gram-schmidt is x =\n{}'.format(x))
print('\nThe two solutions are equal within and error of {}: {}'.format(rtol, np.allclose(x, x_gs, rtol)))

##################
#### PART C.3 ####
##################

# Check of inverse
B = gv_inverse(θ)
AB = np.dot(A,B)
print('\n\nPART C.3: Check of inverse by Givens-rotations')
print('AB == I with an error of {}: {}'.format(rtol, np.allclose(AB, np.identity(n), rtol)))
