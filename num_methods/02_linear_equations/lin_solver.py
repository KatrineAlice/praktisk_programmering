import numpy as np

def back_sub(R, b):
    """
    In-place back-substitution. See page 2 chapter linear equations
    """
    n, m = R.shape
    list = range(m)
    for i in list[::-1]:
        b[i] /= R[i, i]
        for k in range(i+1, m):
            b[i] -= b[k] * R[i, k] / R[i, i]

def gs_decomp(A, R):
    """
    The wanted decomp. is A = QR. Here A is substituted with Q and returned with R
    """
    # assigning n and m, and checking that the dimensions are correct
    n, m = A.shape
    l, k = R.shape
    assert n >= m
    assert m == l
    assert l == k

    # now to the Gram-Schmidt. See bottom of page 3 in chapter 'Systems of lin. eqs.'
    for i in range(m):
        R[i,i] = np.sqrt(np.dot(A[:,i],A[:,i]))
        # here A is substituted by Q
        A[:,i] /= R[i,i]
        for j in range (i+1,m):
            R[i,j] = np.dot(A[:,i],A[:,j])
            A[:,j] -= A[:,i]*R[i,j] 
            # making R upper triangular
            R[j, i] = 0

    return A, R

def gs_solver(Q, R, b):
    """
    Solves QRx = b by substituting b with Q^T b whereafter doing back-substitution. b is substituted with the solution x
    """
    b[:] = np.dot(Q.T, b)
    back_sub(R, b)

    return b

def gs_inverse(Q, R):
    """
    Calculates the inverse of Q, Q^{-1} = B
    """
    n, m = Q.shape
    assert n == m

    B = np.identity(m)
    for i in range(m):
        gs_solver(Q, R, B[:, i])

    return B

def gv_decomp(A):
    """
    Decomposes matrix A by the Givens rotations. 
    The angles (θ) is stored in A
    See page 8 in chapter linear equations 
    """
    n, m = A.shape
    assert n >= m
    for q in range(m):
        for p in range(q+1, n):
            θ = np.arctan2(A[p, q], A[q, q])
            for k in range(q, m):
                xq = A[q, k]
                xp = A[p, k]
                A[q, k] = xq*np.cos(θ) + xp*np.sin(θ)
                A[p, k] = -xq*np.sin(θ) + xp*np.cos(θ)
            A[p, q] = θ
    return A

def gv_solver(A, b):
    """
    Solves Ax = b after A has been decomposed by givens rotations
    """
    n, m = A.shape
    for q in range(m):
        for p in range(q+1, n):
            θ = A[p, q]
            xq = np.copy(b[q])
            xp = np.copy(b[p])
            b[q] = xq*np.cos(θ) + xp*np.sin(θ)
            b[p] = -xq*np.sin(θ) + xp*np.cos(θ)
    back_sub(A, b)
    return b

def make_R(A):
    """
    Constructs R from A = QR 
    (Used for ../4_least_squares exercises)
    """
    n, m = A.shape
    R = np.zeros((m,m), dtype='float64')

    for i in range(m):
        for j in range(i + 1):
            R[j, i] = A[j, i]
    return R

def gv_inverse(A):
    """
    Calculates the inverse of square matrix A. A^{-1}=B
    """
    n, m = A.shape
    assert n == m
    B = np.identity(m)
    for i in range(m):
        gv_solver(A, B[:, i])
    return B
