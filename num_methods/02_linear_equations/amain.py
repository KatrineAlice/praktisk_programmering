import numpy as np
from lin_solver import gs_decomp, gs_solver

# To make sure the same seed of random numbers are used in each run:
np.random.seed(20)
rtol = 1e-20

##################
#### PART A.1 ####
##################

# Create a random tall matrix (n > m) 
n = 7
m = 5

A_init = np.random.rand(n,m)
A = np.copy(A_init)
R = np.random.rand(m,m)

# Check of gram-schmidt decomposition
Q, R = gs_decomp(A=A_init, R=R)
QR = np.dot(Q,R)
QTQ = np.dot(Q.T, Q)

print('PART A.1: Check of decomposition of A into Q and R:')
print('A has dimensions {}x{} and A =\n{}'.format(*A.shape, A))
print('\nFrom the decomposition: Q =\n{}'.format(Q))
print('\nQ must be orthogonal. This is fulfilled when Q^T Q = I')
print('Q^T * Q == I within an error of {}: {}'.format(rtol, np.allclose(QTQ, np.identity(m))))
print('\nR should be triangular, R =\n{}'.format(R))
print('\nQR == A within an error of {}: {}'.format(rtol, np.allclose(QR, A, rtol)))

##################
#### PART A.2 ####
##################

# Create random square matrices (n x n) A, R and vector b
n = 5

A_init = np.random.rand(n, n)
A = np.copy(A_init)
R = np.random.rand(n, n)
b_init = np.random.rand(n)
b = np.copy(b_init)

# Check of gram-schmidt solver
Q, R = gs_decomp(A=A_init, R=R)
x = gs_solver(Q=Q, R=R, b=b_init)
Ax = np.dot(A, x)

print('\n\nPART A.2: Check of gram-schmidt solver')
print('A has dimensions {}x{} and A =\n{}'.format(*A.shape, A))
print('\nThe solution after decomposition and back-substitution is x=\n{}'.format(x))
print('\nAx == b within an error of {}: {}'.format(rtol,np.allclose(Ax, b)))
