import numpy as np
from monte_carlo import plain
from inspect import *
import sys
import time

def f1(x):
    return np.sin(x[0]) * x[1]

def f2(x):
    if x[0]*x[0] + x[1]*x[1] < 1:
        return 1
    else:
        return 0

def f3(x):
    return (1/(1 - np.cos(x[0])*np.cos(x[1])*np.cos(x[2])))/np.pi**3


def mc(function, a, b, N, exact):
    """
    Calls the monte carlo integrator
    """
    # timing for multi processing assignment
    t0 = time.time()

    # Monte carlo integration
    print(f'\n\nIntegrating function defined as:\n')
    print(getsource(function))
    print(f'\nIntegrating  from {a} to {b} ...\n')
    integ, error = plain(function, a, b, N)
    print('Done.\n')

    # Print result:
    print('---------------------------------------------')
    print('Result:')
    print('---------------------------------------------')
    #print(f'num. of calls:\t\t {global_ncalls.ncalls}')
    print(f'Sampling, N:\t\t{N}')
    print(f'Integral:\t\t{integ}')
    print(f'Exact:\t\t\t{exact}')
    print(f'Estimated error:\t{error}')
    print(f'Actual error:\t\t{abs(integ-exact)}')
    print(f'Time used:\t\t{time.time()-t0:.3}')
    print('---------------------------------------------')

def error_est(function, a, b):
    # Timing for multi processing assignment
    t0 = time.time()

    print('\n\nError estimate on function defined as:\n')
    print(getsource(function))
    print('\nSaving output in aberr.out ...')
    print('# Error estimate', file=sys.stderr)
    for i in range(1,7):
        N = int(10**i)
        integ, error = plain(function, a, b, N)
        print(f'{N}\t{error}', file=sys.stderr)
    print('\n', file=sys.stderr)
    print('\nDone.')
    print(f'\nTime used:\t{time.time()-t0:.3}')

def amain():
    # Timing for multi-processing assignment
    t = time.time()

    # Parameters
    f = ([f1, f2, f3])
    a = np.array([np.array([0, 0]), np.array([0, 0]), np.array([0, 0, 0])])
    b = np.array([[np.pi/2., np.pi], [1, 1], [np.pi, np.pi, np.pi]])
    N = ([1000, 50000, 100000])
    exact = ([(np.pi*np.pi)/2, np.pi/4, 1.3932039296856768591842462603255])

    # Integrate (part a)
    for i in range(len(f)):
        mc(f[i], a[i], b[i], N[i], exact[i])

    print(f'\n\nThe total time used for part A:\t{time.time()-t}')

def bmain():
    # Timing for multi-processing assignment
    t = time.time()

    # Parameters
    f = ([f1, f2, f3])
    a = np.array([np.array([0, 0]), np.array([0, 0]), np.array([0, 0, 0])])
    b = np.array([[np.pi/2., np.pi], [1, 1], [np.pi, np.pi, np.pi]])
    N = ([1000, 50000, 100000])
    exact = ([(np.pi*np.pi)/2, np.pi/4, 1.3932039296856768591842462603255])

    # Error estimate (part b)
    for i in range(len(f)-1):
        print(f'# Testing on function f{i+1} ...', file=sys.stderr)
        error_est(f[i], a[i], b[i])

    print(f'\n\nThe total time used for part B:\t{time.time()-t}')


if __name__ == '__main__':
    if sys.argv[1:][0] == 'A':
        amain()
    elif sys.argv[1:][0] == 'B':
        bmain()
