import numpy as np

def random_x(a,b):
    random = np.random.rand(1)
    return [a[i] + random*(b[i] - a[i]) for i in range(len(a))]

def plain(f, a, b, N):
    """
    Plain monte carlo integrator. 
    Parameters:
        f: function to be integrated
        a: lower limit
        b: upper limit
    """
    # Initialise
    volume = 1
    sum1 = 0
    sum2 = 0
    x = np.zeros(a.shape, dtype='float64')

    # Calculate volume
    for i in range(len(a)):
        volume *= b[i] - a[i]

    for i in range(N):
        #x = random_x(a,b)
        x = np.random.uniform(a, b, len(a)) # more robust than random_x
        fx = f(x)
        sum1 += fx
        sum2 += fx*fx

    # Caculate the mean and the variance
    mean = sum1/N
    sigma = np.sqrt(sum2/N - mean*mean)
    
    # Calculate error by central limit theorem
    error = volume*sigma/np.sqrt(N)
    integ = mean*volume

    return integ, error

