import numpy as np
from scipy import integrate
import sys
import global_ncalls
import functions
from set_function import set_function
from adapt import adapt


def QUAD(function):
    """
    Calls the adaptive integrator on the chosen function
    """
    # Initialise
    global_ncalls.ncalls = 0
    acc = 1e-03
    eps = 1e-03
    
    # Set function
    f, exact, a, b = set_function(function)
    if f == 0:
        print('\nIntegrating unknown function ... stop.')
        return
    
    # Adaptive integration
    print(f'\n\nIntegrating {function} from {a} to {b} ...\n')
    Q = adapt(f, a, b, acc, eps)
    print('Done.\n')

    # Integrate by Scipy for comparison
    print('Integrating by scipy.integrate.quad for comparison ...\n')
    compare, cerr = integrate.quad(f, a, b) 
    print('Done.\n')

    # Print result:
    print('--------------------------------------------')
    print('Result:')
    print('--------------------------------------------')
    print(f'acc: \t\t\t {acc}')
    print(f'eps: \t\t\t {eps}')
    print(f'num. of calls:\t\t {global_ncalls.ncalls}')
    print(f'integral:\t\t{Q}')
    print(f'exact:\t\t\t{exact}')
    print(f'Estimated err:\t\t{acc+abs(Q)*eps:15.16f}')
    print(f'Actual err:\t\t{abs(Q - exact):15.16f}')
    print(f'Scipy quad:\t\t{compare}')
    print(f'Scipy error:\t\t{cerr}')
    print('--------------------------------------------')


# MAIN
# Limit no. of recursions
sys.setrecursionlimit(15000)

# Possible functions to integrate
flist = (['sqrt(x)', '1/sqrt(x)', 'log(x)/sqrt(x)', '4*sqrt(1-(1-x)^2)'])

# Integrate
print('\nRun test: Adaptive open24 integration ...')
for fs in flist:
    QUAD(fs)
