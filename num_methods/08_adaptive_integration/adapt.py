import numpy as np

def points_weights():
    """
    xi: easily reusable points
    wi: higher order rule: trapezium
    vi: lower order rule: rectangle
    """
    xi = np.array([1/6., 2/6., 4/6., 5/6.])
    wi = np.array([2/6., 1/6., 1/6., 2/6.])
    vi = np.array([1/4., 1/4., 1/4., 1/4.])

    return xi, wi, vi

def open4(f, a, b, f1, f2, acc, eps):
    """
    Routine to integrate f with reusable points and a fourth 
    order trapezium rule. => Q
    A lower order rectangle rule is used in order to 
    calculate the local error. => q and err
    Parameters:
        f: integrand
        a: start point
        b: end point
        f1: Reused point 
        f2: Reused point
        acc: Absolute accuracy
        eps: Relative accuracy
    """
    # Initialise
    xi, wi, vi = points_weights()
    wi *= (b - a)
    vi *= (b - a)

    # Calculate nodes
    f0 = f(a + xi[0]*(b - a))
    f3 = f(a + xi[3]*(b - a))

    # Q, q is calculated
    Q = (f0*wi[0] + f1*wi[1] + f2*wi[2] + f3*wi[3])
    q = (f0*vi[0] + f1*vi[1] + f2*vi[2] + f3*vi[3])

    # Estimate tolerance and calculate error
    tol = acc + abs(Q)*eps
    #err = abs(Q - q)/2 # optimistic?
    err = abs(Q - q) # more precise result

    if err < tol:
        return Q
    else:
        half = (a + b)/2.
        QA = open4(f, a, half, f0, f1, acc/np.sqrt(2), eps)
        QB = open4(f, half, b, f2, f3, acc/np.sqrt(2), eps)
        return QA + QB

def adapt(f, a_init, b_init, acc, eps):
    """
    Adaptive open integration with reusable points.
    
    Parameters:
        f: integrand
        a: start point
        b: end point
        acc: Absolute accuracy
        eps: Relative accuracy

    Update:
    The routine will soon  handle infinite limits
    """
    assert(a_init < b_init)

    # Initialise
    xi, _, _ = points_weights()
    a = a_init
    b = b_init
    
    # Handle possibly infinite limits (eqs. 58, 60, 62)
    if np.any(np.isinf([a_init, b_init])):
        F = f 
        if a_init is np.NINF and b_init is not np.PINF:
            f = lambda t: (F(b_init - (1 - t)/t))/t**2
            a = 0
            b = 1
        elif a_init is not np.NINF and b_init is np.PINF:
            f = lambda t: (F(a_init + (1 - t)/t))/t**2
            a = 0
            b = 1
        else:
            f = lambda t: (F((1 - t)/t) + F(- (1 - t)/t))/t**2
            a = 0
            b = 1

    # Estimate by nodes (indexed from 0)
    f1 = f(a + xi[1]*(b - a))
    f2 = f(a + xi[2]*(b - a))

    return open4(f, a, b, f1, f2, acc, eps)
