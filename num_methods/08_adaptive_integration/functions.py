import numpy as np
import global_ncalls

# Functions to be integrated
def f1(x):
    global_ncalls.ncalls += 1
    return np.sqrt(x)

def f2(x):
    global_ncalls.ncalls +=1
    return 1./np.sqrt(x)

def f3(x):
    global_ncalls.ncalls += 1
    return np.log(x)/np.sqrt(x)

def f4(x):
    global_ncalls.ncalls += 1
    return 4*np.sqrt(1 - (1 - x)**2)

def f5(x):
    global_ncalls.ncalls += 1
    return np.exp(-x)*np.sin(x)

def f6(x):
    global_ncalls.ncalls += 1
    return np.exp(-(x**2))

def f7(x):
    global_ncalls.ncalls += 1
    return np.exp(-2*(x**2))

def f8(x):
    global_ncalls.ncalls += 1
    return (np.sin(x)*np.sin(x))/(x*x)

def f9(x):
    global_ncalls.ncalls += 1
    return np.exp(3*x)
