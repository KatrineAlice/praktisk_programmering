import numpy as np
import sys
import global_ncalls
import functions
from adapt import adapt

def set_function(function):
    if function.lower() == 'sqrt(x)':
        f = functions.f1
        limits = 0, 1
        exact = 2/3
    
    elif function.lower() == '1/sqrt(x)':
        f = functions.f2
        limits = 0, 1
        exact = 2

    elif function.lower() == 'log(x)/sqrt(x)':
        f = functions.f3
        limits = 0, 1
        exact = -4
    
    elif function.lower() == '4*sqrt(1-(1-x)^2)':
        f = functions.f4
        limits = 0, 1
        exact = np.pi

    elif function.lower() == 'exp(-x)*sin(x)':
        f = functions.f5
        limits = 0, np.PINF
        exact = 1/2

    elif function.lower() == 'exp(-x^2)':
        f = functions.f6
        limits = np.NINF, np.PINF
        exact = np.sqrt(np.pi)

    elif function.lower() == 'exp(-2x^2)':
        f = functions.f7
        limits = 0, np.PINF
        exact = np.sqrt(np.pi/2)/2
    
    elif function.lower() == 'sin^2(x)/x^2':
        f = functions.f8
        limits = np.NINF, 0
        exact = np.pi/2
    
    elif function.lower() == 'exp(3*x)':
        f = functions.f9
        limits = np.NINF, 0
        exact = 1/3

    else:
        f = 0
        exact = 0
        limits = 0, 0
    
    a, b = limits
    return f, exact, a, b
