#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>

/* https://www.gnu.org/software/gsl/doc/html/multimin.html?highlight=minimize#examples */

double rosenbrock (const gsl_vector* a, void* params){
    double x = gsl_vector_get(a,0);
    double y = gsl_vector_get(a,1);

    return (1-x)*(1-x) + 100*(y-x*x)*(y-x*x);
}


int main(){
    const int dim = 2;
  
    const gsl_multimin_fminimizer_type * T = gsl_multimin_fminimizer_nmsimplex2;
    gsl_multimin_fminimizer * s = gsl_multimin_fminimizer_alloc(T,dim);

    gsl_multimin_function F;
    F.f = rosenbrock;
    F.n = dim;

    gsl_vector* init = gsl_vector_alloc(dim);
    gsl_vector_set(init,0,2);
    gsl_vector_set(init,1,2);

    gsl_vector* step = gsl_vector_alloc(dim);
    gsl_vector_set(step,0,0.1);
    gsl_vector_set(step,1,0.1);

    gsl_multimin_fminimizer_set(s,&F,init,step);

    int iter = 0, status;
    do{
        iter++;
        status = gsl_multimin_fminimizer_iterate(s);

        if (status)
          break;

        // PRINT OF THE PROCESS TO STDERR
        double x = gsl_vector_get(s->x,0);
        double y = gsl_vector_get(s->x,1);
        fprintf(stderr,"%g %g\n",x,y);
        
        status = gsl_multimin_test_size(s->size,1e-2);
        if(status==GSL_SUCCESS)
          break;
    }while(status == GSL_CONTINUE && iter < 100);

    // PRINT OF THE FINAL RESULT TO STDOUT
    double x = gsl_vector_get(s->x,0);
    double y = gsl_vector_get(s->x,1);
    printf("x_min%g y_min=%g iterations=%i\n",x,y,iter);

    gsl_vector_free(init);
    gsl_vector_free(step);
    gsl_multimin_fminimizer_free(s);

return 0;
}
