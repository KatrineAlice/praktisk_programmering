#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_vector.h>
#include<assert.h>

struct experimental_data{int n; double *t, *y, *e;};

double function_to_minimize(const gsl_vector *x, void *params){
    double A = gsl_vector_get(x,0);
    double T = gsl_vector_get(x,1);
    double B = gsl_vector_get(x,2);
    struct experimental_data *p = (struct experimental_data*) params;
    int n = p->n;
    double *t = p->t;
    double *y = p->y;
    double *e = p->e;
    double sum = 0;
#define f(t) A*exp(-(t)/T)+B
    for(int i=0;i<n;i++) sum += pow((f(t[i]) - y[i])/e[i], 2);
    return sum;
}

int main(){

double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};

    int n = sizeof(t)/sizeof(t[0]);
    assert(n==sizeof(y)/sizeof(y[0]) && n==sizeof(e)/sizeof(e[0]));

    fprintf(stderr,"# t[i], y[i], e[i]\n");
    for(int i=0;i<n;i++) fprintf(stderr,"%g %g %g\n",t[i],y[i],e[i]);
    fprintf(stderr,"\n\n");


    struct experimental_data params;
    params.n = n;
    params.t = t;
    params.y = y;
    params.e = e;

    int dim = 3;
    gsl_multimin_function F;
    F.f = function_to_minimize;
    F.n = dim;
    F.params = (void*)&params;

    gsl_vector *init = gsl_vector_alloc(dim);
    gsl_vector_set(init,0,3);
    gsl_vector_set(init,1,2);
    gsl_vector_set(init,2,0);

    gsl_vector *step = gsl_vector_alloc(dim);
    gsl_vector_set(step,0,2);
    gsl_vector_set(step,1,2);
    gsl_vector_set(step,2,2);


    const gsl_multimin_fminimizer_type * K = gsl_multimin_fminimizer_nmsimplex2;
    gsl_multimin_fminimizer * s = gsl_multimin_fminimizer_alloc(K,dim);
    gsl_multimin_fminimizer_set(s,&F,init,step);

    int iter = 0, status;
    do{
        iter++;
        status = gsl_multimin_fminimizer_iterate(s);
        if(status) break;

        status = gsl_multimin_test_size(s->size,1e-2);
        if(status==GSL_SUCCESS) break; 
    }while(status==GSL_CONTINUE && iter < 100); 
    
    double A = gsl_vector_get(s->x,0);
    double T = gsl_vector_get(s->x,1);
    double B = gsl_vector_get(s->x,2);

    double dt = (t[n-1]-t[0])/50;
    for(double ti=t[0]; ti<t[n-1]+dt; ti+=dt) printf("%g %g\n",ti,f(ti));
    
    fprintf(stderr,"# A=%g T=%g B=%g iter=%i\n",A,T,B,iter);

    gsl_multimin_fminimizer_free(s);
    gsl_vector_free(init);
    gsl_vector_free(step);

return 0;
}
