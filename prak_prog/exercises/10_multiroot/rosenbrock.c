#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_errno.h>

int rosenbrock_f (const gsl_vector* a, void* params, gsl_vector* f){
    const double x = gsl_vector_get(a,0);
    const double y = gsl_vector_get(a,1);
    // the derivatives:
    const double fx = -2*(1-x) - 400*(y-x*x)*x;
    const double fy = 200*(y-x*x);

    gsl_vector_set (f, 0, fx);
    gsl_vector_set (f, 1, fy);

return GSL_SUCCESS;
}

int rosenbrock(double* xmin, double* ymin, int* iteration){
    const gsl_multiroot_fsolver_type *T = gsl_multiroot_fsolver_hybrids;
    gsl_multiroot_fsolver *s = gsl_multiroot_fsolver_alloc(T,2);

    gsl_multiroot_function F;
    F.f = rosenbrock_f;
    F.n = 2;
    F.params = NULL;

    gsl_vector *init = gsl_vector_alloc(2);
    gsl_vector_set(init,0,0);
    gsl_vector_set(init,1,3);

    gsl_multiroot_fsolver_set(s, &F, init);

    int status, iter = 0;
    do {
	iter++;
	status = gsl_multiroot_fsolver_iterate(s);
	if (status)
	  break;
        status = gsl_multiroot_test_residual(s->f, 1e-6);
    }

    while (status == GSL_CONTINUE);

    *xmin = gsl_vector_get(s->x,0);
    *ymin = gsl_vector_get(s->x,1);
    *iteration = iter;

    gsl_multiroot_fsolver_free(s);
    gsl_vector_free(init);

return 0;
}
