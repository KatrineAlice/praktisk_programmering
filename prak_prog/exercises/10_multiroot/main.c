#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>
#include<assert.h>
#define FSOLVER gsl_multiroot_fsolver_broyden

double rosenbrock();
double Fe_hydrogen(double e, double r);

// AUXILLIARY FUNCTION, HYDROGEN
int aux(const gsl_vector *x, void *params, gsl_vector *f){
    double e=gsl_vector_get(x,0);
    assert(e<0);
    double rmax=*(double*)params;
    double fval=Fe_hydrogen(e,rmax);
    gsl_vector_set(f,0,fval);
    return GSL_SUCCESS;
}

int main(){
    // ROSENBROCK
    double xmin, ymin;
    int iteration;
    rosenbrock(&xmin, &ymin, &iteration);
    printf("# The minimum of the Rosenbrock func. is (x,y)= (%g, %g)\n",xmin,ymin);
    printf("# Number of iterations: %i \n\n",iteration);

    // HYDROGEN
    double rmax = 8;
    fprintf(stderr,"rmax = %g\n",rmax);

    int dimension = 1;
    gsl_multiroot_fsolver * solver =
	gsl_multiroot_fsolver_alloc (FSOLVER, dimension);

    gsl_multiroot_function F;
    F.f = aux;
    F.n = dimension;
    F.params = (void*)&rmax;

    // start guess
    gsl_vector *init = gsl_vector_alloc(dimension);
    gsl_vector_set(init,0,-1);

    gsl_multiroot_fsolver_set(solver, &F, init);

    int status, iter=0;
    const double epsabs=1e-3;
    do{
	iter++;
	status = gsl_multiroot_fsolver_iterate(solver);
	if(status)
	    break;
	status = gsl_multiroot_test_residual(solver->f,epsabs);
	if(status==GSL_SUCCESS)fprintf(stderr,"converged\n");

	fprintf(stderr,"iter= %3i ",iter);
	fprintf(stderr,"e= %10g ",gsl_vector_get(solver->x,0));
	fprintf(stderr,"f(rmax)= %10g ",gsl_vector_get(solver->f,0));
	fprintf(stderr,"\n");
    }while( status == GSL_CONTINUE && iter < 100);

    double e=gsl_vector_get(solver->x,0);
    printf("# rmax, e\n");
    printf("%g %g\n",rmax,e);
    printf("\n\n");

    printf("# r, Fe_hydrogen(e,r), exact\n");
//	for(double r=0; r<=rmax; r+=rmax/64) printf("%g %g %g\n",r,Fe(e,r),r*exp(-r)*Fe(e,1)*exp(1));

    for(double r=0; r<=rmax; r+=rmax/64){
	 printf("%.4g\t %.9g\t %.8g\n",r,Fe_hydrogen(e,r),r*exp(-r));}

    gsl_multiroot_fsolver_free(solver);
    gsl_vector_free(init);

return 0;
}
