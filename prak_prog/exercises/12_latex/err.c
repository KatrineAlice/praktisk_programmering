#include<math.h>
#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int err_diff(double x, const double y[], double dydx[], void * params)
{
    dydx[0] = (2.0/sqrt(M_PI)*exp(-x*x));
    return GSL_SUCCESS;
}

double err_fkt(double x){

    gsl_odeiv2_system sys;
    sys.function = err_diff;
    sys.jacobian = NULL;
    sys.dimension=1;
    sys.params=NULL;

    double hstart=copysign(0.1,x);
    double acc=1e-6;
    double eps=1e-6;

    gsl_odeiv2_driver* driver=gsl_odeiv2_driver_alloc_y_new (&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);

    double t=0;
    double y[]={0}; // start conditions
  
    gsl_odeiv2_driver_apply(driver,&t,x,y);
    gsl_odeiv2_driver_free(driver);

    return y[0];
}

int main(int argc, char const *argv[]){
    double a = atof(argv[1]);
    double b = atof(argv[2]);
    double dx = atof(argv[3]);

    for(double x=a; x<b; x+=dx)
    printf("%g\t %g\n", x, err_fkt(x));

return 0;
}
