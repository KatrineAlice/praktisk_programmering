#include <stdio.h>
#include <math.h>

int main(void){
	float a = 0.1111111111111111111111111111;
	double b = 0.1111111111111111111111111111;
	long double c = 0.1111111111111111111111111111L;

	printf("f_num = %.25g\n",a);
	printf("d_num = %.25lg\n",b);
	printf("ld_num = %.25Lg\n",c);
}
