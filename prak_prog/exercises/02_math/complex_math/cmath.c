#include <stdio.h>
#include <math.h>
#include <complex.h>

int main(void){
	double complex b = csqrt(-2);
	printf("sqrt(-2)=%g + i%g\n",creal(b),cimag(b));
	
	double complex c = cexp(I);
	printf("exp(i)=%g + i%g\n",creal(c),cimag(c));

	double complex d = cexp(I*M_PI);
	printf("exp(i*pi)=%g + i%g\n",creal(d),cimag(d));

	double complex e = cpow(I,M_E);
	printf("i^e=%g+i%g\n",creal(e),cimag(e));
}
