#include <stdio.h>
#include <math.h>
#include <gsl/gsl_integration.h>

double function (double x, void * params) {
    double function = log(x) / sqrt(x);
    return function;
}

int main (void){
    gsl_integration_workspace * w = gsl_integration_workspace_alloc (1000);

    double expected = -4.0;
    int a = 0, b = 1;
    double epsabs = 1e-6, epsrel = 1e-6;
    double result, error;

    gsl_function F;
    F.function = &function;
    F.params = 0;

    gsl_integration_qags (&F, a, b, epsabs, epsrel, 1000, w, &result, &error);

    printf ("result          = % .18f\n", result);
    printf ("exact result    = % .18f\n", expected);
    printf ("estimated error = % .18f\n", error);
    printf ("actual error    = % .18f\n", result - expected);

    gsl_integration_workspace_free (w);

return 0;
}
