#include<math.h>
#include<stdio.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

double psi2_integrand(double x, void * params){
    double alpha = *(double *) params;
    double psi2 = exp(-alpha*x*x);
    return psi2;
}

double psi2(double alpha){
    int limit = 100;
    gsl_integration_workspace* w = gsl_integration_workspace_alloc(limit);
    
    gsl_function F;
    F.function = &psi2_integrand;
    F.params = &alpha;

    double epsabs = 1e-6, epsrel = 1e-6, result, error;
    int status = gsl_integration_qagi(&F, epsabs, epsrel, limit, w, &result, &error);

    gsl_integration_workspace_free(w);

    if(status!=GSL_SUCCESS) return NAN;
    else return result;
}
