#include<math.h>
#include<stdio.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

double hamilton_integrand(double x, void * params){
    double alpha = *(double*) params;
    double ham_int = (-alpha*alpha*x*x/2 + alpha/2 + x*x/2)*exp(-alpha*x*x);
    return ham_int;
}

double hamilton(double alpha){
    int limit = 100;
    gsl_integration_workspace* w = gsl_integration_workspace_alloc(limit);
    
    gsl_function F;
    F.function = &hamilton_integrand;
    F.params = &alpha;

    double epsabs = 1e-6, epsrel = 1e-6, result, error;
    int status = gsl_integration_qagi(&F, epsabs, epsrel, limit, w, &result, &error);

    gsl_integration_workspace_free(w);

    if(status!=GSL_SUCCESS) return NAN;
    else return result;
}

