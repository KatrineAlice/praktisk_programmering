#include<stdio.h>
#include<math.h>

double psi2(double alpha);
double hamilton(double alpha);

int main(){
// TEST OF FUNCTIONS.    
    //printf("# psi2(1) = %g\n", psi2(1.0));
    //printf("# hamilton(1) = %g\n", hamilton(1.0));

// DATA
    for(double x=0.01; x<10; x+=0.1){
	printf("%g %g\n",x,hamilton(x)/psi2(x));

    }
return 0;
}
