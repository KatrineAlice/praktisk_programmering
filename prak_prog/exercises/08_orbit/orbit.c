#include<math.h>
#include<stdio.h>
#include<getopt.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int orb_equation(double phi, const double *y, double *yprime, void *params){
    double epsilon = *(double *) params;
    yprime[0]=y[1];
    yprime[1] = 1 - y[0] + epsilon*y[0]*y[0];
 
    return GSL_SUCCESS;
}

int main(int argc, char** argv){
//DEFAULT VALUES
    double epsilon = 0, uprime = 0;

//READING PARAMETERS FROM COMMAND-LINE
    while(1){
	int opt = getopt(argc, argv, "e:p:");
	if(opt == -1) break;
	switch(opt){
	    case 'e': epsilon = atof(optarg); break;
	    case 'p': uprime = atof(optarg); break;
	    default:
	    fprintf(stderr, "usage: %s [-e epsilon] [-p uprime]\n", argv[0]);	
	    exit(EXIT_FAILURE);
	}
    }

//ODE
    gsl_odeiv2_system orbit;
    orbit.function = orb_equation;
    orbit.jacobian = NULL;
    orbit.dimension= 2;
    orbit.params= (void*) &epsilon;

    // LIMITS FOR ODE: ERRORS AND PARAMETERS
    double hstart = 1e-3, epsabs = 1e-6, epsrel = 1e-6;
    double phi_max = 39.5 * M_PI, delta_phi = 0.05;

    gsl_odeiv2_driver* driver = gsl_odeiv2_driver_alloc_y_new(&orbit, gsl_odeiv2_step_rk8pd, hstart, epsabs, epsrel);
    
    // START CONDITION
    double t = 0, y[2] = { 1, uprime };
    for (double phi = 0; phi < phi_max; phi += delta_phi){
	int status = gsl_odeiv2_driver_apply (driver,&t,phi,y);
	printf("%g %g\n", phi, y[0]);
	if (status != GSL_SUCCESS) fprintf(stderr, "fun: status=%i", status);

	}

  gsl_odeiv2_driver_free(driver);
return 0;
}
