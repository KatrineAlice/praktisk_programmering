#include<stdio.h>
#include<math.h>
#include<gsl/gsl_sf_airy.h>
#include<gsl/gsl_sf.h>

int main(){
    gsl_mode_t acc = GSL_PREC_DOUBLE; 
    for(double x=-5; x<5; x+=0.025)
	printf("%g\t %g\t %g\n", x, gsl_sf_airy_Ai(x,acc), gsl_sf_airy_Bi(x,acc));

return 0;
}
