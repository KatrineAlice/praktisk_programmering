#include<stdio.h>
#include<gsl/gsl_linalg.h>
#include<gsl/gsl_blas.h>

/* This program solves the linear equation using LU-decomposition. 
   There are other methods for solving linear equations. See 
   http://86.52.112.181/~fedorov/prog/exercise-gsl.htm and the GSL-manual */

int main (void){
    double a_data[] = { 6.13, -2.90,  5.86,
                        8.08, -6.31, -3.89,
                       -4.36,  1.00,  0.19 };

    double b_data[] = { 6.23,  5.37, 2.29 };

    printf("A =\n%g\t %g\t %g \n%g\t %g\t %g \n%g\t %g\t %g\n",
            a_data[0], a_data[1], a_data[2], a_data[3], a_data[4],
            a_data[5], a_data[6], a_data[7], a_data[8]);
    printf("b = \n%g \n%g \n%g \n", b_data[0], b_data[1], b_data[2]); 
    
    gsl_matrix_view a = gsl_matrix_view_array (a_data, 3, 3);
    gsl_vector_view b = gsl_vector_view_array (b_data, 3);
    gsl_vector *x = gsl_vector_alloc(3);
    
    int s;
    gsl_permutation * p = gsl_permutation_alloc(3);
    gsl_linalg_LU_decomp(&a.matrix, p, &s);
    gsl_linalg_LU_solve(&a.matrix, p, &b.vector, x);

    printf("x = \n");
    gsl_vector_fprintf(stdout, x, "%g");

// check solution
    double A_data[] = { 6.13, -2.90,  5.86,
                        8.08, -6.31, -3.89,
                       -4.36,  1.00,  0.19 };
    gsl_matrix_view A = gsl_matrix_view_array(A_data, 3, 3);
    gsl_vector *C = gsl_vector_alloc(3);
    gsl_blas_dgemv(CblasNoTrans, 1, &A.matrix, x, 0, C);
    printf("A*x = (should be equal to b)\n");
    gsl_vector_fprintf(stdout, C, "%g");


// Remember to free memory! 
    gsl_permutation_free(p);
    gsl_vector_free(x);
    gsl_vector_free(C);

return 0;
}
