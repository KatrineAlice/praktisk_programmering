#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_errno.h>
#include<assert.h>

int arccot(const gsl_vector* v, void *params, gsl_vector *f){
    double a = gsl_vector_get(v, 0);	
    double x = *(double*) params;
    assert(x>0);
    gsl_vector_set(f, 0, 1./tan(a)-x);
    return GSL_SUCCESS;
}

int arctan(const gsl_vector* v, void* params, gsl_vector* f){
    double a = gsl_vector_get(v, 0);
    double x = *(double*) params;
    assert(x>0);
    gsl_vector_set(f, 0, (tan(a)-x));
    return GSL_SUCCESS;
}

double my_arctan(double x){
    if(x < 0) return -my_arctan(-x);
    //if(x > 1) return M_PI/2 - my_arccot(x);
    assert(x > 0);
    //assert(x <= 1);

    int dim = 1;
    gsl_multiroot_function F;
    F.f = arctan;
    F.n = dim;
    F.params = (void*) &x;
	
    gsl_vector* init = gsl_vector_alloc(dim);
    gsl_vector_set(init, 0, 1);
	
    const gsl_multiroot_fsolver_type *T =
        gsl_multiroot_fsolver_hybrid;   
    gsl_multiroot_fsolver *s =
        gsl_multiroot_fsolver_alloc(T, dim);
	
    gsl_multiroot_fsolver_set(s, &F, init);
    int status;
    int iter = 0;
    double acc = 1e-6;
    do{
        iter++;
        status = gsl_multiroot_fsolver_iterate(s);
        if (status)
            break;
        status = gsl_multiroot_test_residual (s->f, acc);
        }
    while(status == GSL_CONTINUE && iter < 1000);
    if(iter == 1000)
        printf("WARNING: 1000 iter. reached\n");
	
    double result = fmod(gsl_vector_get(s->x, 0), M_PI/2);
    while(result < 0) result += M_PI/2;

    gsl_multiroot_fsolver_free(s);
    gsl_vector_free(init);

    return result;	
}

double my_arccot(double x){
    if (x < 0.) return M_PI - my_arccot(-x);
    if (x <= 1) return M_PI/2 - my_arctan(x);
    assert(x > 0.);
    assert(x > 1.);

    int dim = 1;

    gsl_multiroot_function F;
    F.f = arccot;
    F.n = 1;
    F.params = (void*) &x;
	
    gsl_vector* init = gsl_vector_alloc(dim);
    gsl_vector_set(init, 0, 1);

    const gsl_multiroot_fsolver_type *T = gsl_multiroot_fsolver_hybrid;
    gsl_multiroot_fsolver *s = gsl_multiroot_fsolver_alloc(T, dim);

    gsl_multiroot_fsolver_set(s, &F, init);
    int status, iter = 0;
    double acc = 1e-6;
    do{
        iter++;
        status = gsl_multiroot_fsolver_iterate(s);
        if (status)
            break;		
        status = gsl_multiroot_test_residual (s->f, acc);
    }

    while(status == GSL_CONTINUE && iter < 1000);
    if(iter == 1000)
        printf("WARNING: 1000 iter. reached.\n");
	
    double result = fmod(gsl_vector_get(s->x, 0), M_PI/2);
    while(result < 1e-5) result += M_PI/2;

    gsl_multiroot_fsolver_free(s);
    gsl_vector_free(init);

    return result;	
}


