#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_errno.h>

double my_arccot(double x);
double my_arctan(double x);

int main(void){
    // All data for the arccot function is printed into 
    // file arccot.out by fprintf
    // All data for the arctan function is printed as
    // usual by printf and via the Makefile put into
    // file arctan.out

    FILE* file = fopen("arccot.out","w");	

    // Run my_arcs.c for the two functions separately
    for(double x = -10.; x < 10.; x += 0.2)
        fprintf(file, "%g %g\n", x, my_arccot(x));
    
    for(double x = -10.; x < 10.; x += 0.2)
        printf("%g %g\n", x, my_arctan(x));
    
    fprintf(file, "\n\n");
    printf("\n\n");

    // Print math.h data of functions in separate files
    for(double x = -10.; x < 0; x += 0.2)
        fprintf(file, "%g %g\n", x, M_PI+atan(1/x));
    for(double x = 0.2; x < 10.; x += 0.2)
        fprintf(file, "%g %g\n", x, atan(1/x));
    
    for(double x = -10.; x < 10.; x += 0.2)
        printf("%g %g\n", x, atan(x));

    return 0;
}

