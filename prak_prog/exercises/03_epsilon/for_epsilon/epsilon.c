# include <stdio.h>
# include <math.h>
# include <float.h>

int main(void){
	float x;
	float f = FLT_EPSILON;
	for(x=1; 1+x!=1; x/=2){}
	x*=2;
	int a = (x==f);
	printf("float x=%f\n",x);
	printf("x=machine epsilon, true=%i\n",a);
	

	double y;
	double d = DBL_EPSILON;
	for(y=1; 1+y!=1; y/=2){}
	y*=2;
	int b = (y==d);
	printf("double y=%g\n",y);
	printf("y=machine epsilon, true=%i\n",b);

	long double z=1;
	long double ld = LDBL_EPSILON;
	for(z=1; 1+z!=1; z/=2){}
	z*=2;
	int c = (z==ld);
	printf("long double z=%Lg\n",z);
	printf("z=machine epsilon, true=%i\n",c);

}
