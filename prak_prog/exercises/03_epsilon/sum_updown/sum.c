#include <stdio.h>
#include <math.h>
#include <float.h>
#include <limits.h>

int main(void){
	int max = INT_MAX/2;
	float sum_up_float = 1.0f;
	for(int i=2; i<=max; i++) {sum_up_float+=(1.0f/i);}
	
	float sum_down_float = 1.0f/max;
	for(int j=1; j<max; j++) {sum_down_float+=(1.0f/(max-j));} 

	/*float sum_up_float = 1.0f + 1.0f/2 + 1.0f/3 + ... + 1.0f/max;
	float sum_down_float = 1.0f/max + 1.0f/(max-1) + 1.0f/(max-2) + ... +1.0f;
	*/

	printf("sum up =%f\n",sum_up_float);
	printf("sum down =%f\n\n", sum_down_float);
	
	/*Explain the difference?*/
	if(sum_up_float!=sum_down_float)
	printf("The difference?? Lack of precission in floats?\n");
	printf("For each i it will round off the numbers. Since the calculation\n");
	printf("is done in another order, the rounding is different.\n\n");
	/*Does this some converge as a func. of max?*/
	printf("Converge as a function of max?\n\n");

	double sum_up_double = 1.0;
	for(int k=2; k<=max; k++) {sum_up_double+=(1.0/k);}

	double sum_down_double = 1.0/max;
	for(int l=1; l<max; l++) {sum_down_double+=(1.0/(max-l));} 

	printf("sum up double =%g\n",sum_up_double);
	printf("sum down double =%g\n\n", sum_down_double);
	
	printf("These two seems equal. Confirms the guess from before,\n");
	printf("since doubles have higher precission. But when checked for \n");
	printf("equality it will return false! Perhaps long double is the way to go.\n");



}
