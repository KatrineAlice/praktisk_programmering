# include <stdio.h>
# include <math.h>
# include <limits.h>

int main(void){
	int j=INT_MAX;

	printf("The result with the while loop:\n");
	int i=1;
	while(i+1>i) {i++;}
	printf("my max int = %i\n",i);
	int a = (i==j);
	printf("i==INT_MAX,%i\n",a);

	printf("The result with the for loop:\n");
	int x;
	for (x=1; x+1>x; x++){};
	printf("my max int = %i\n",x);
	int b = (x==j);	
	printf("x==INT_MAX,%i\n",b);
	
	printf("The result with the do-while loop:\n");
	int k=1;
	do {k++;}
	while (k+1>k);
	printf("my max int = %i\n",i);
	int c = (k==j);	
	printf("k==INT_MAX,%i\n",c);
	


}
