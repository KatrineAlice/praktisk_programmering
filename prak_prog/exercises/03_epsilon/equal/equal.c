#include <math.h>
#include <stdlib.h>

int equal(double a, double b, double tau, double epsilon){

	double c = abs(a-b);
	double d = c/(abs(a)+abs(b));

	if(c<tau||d<(epsilon/2)){return 1;}
	else {return 0;}
	

}
