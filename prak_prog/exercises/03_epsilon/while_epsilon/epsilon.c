# include <stdio.h>
# include <math.h>
# include <float.h>

int main(void){
	float x=1;
	float f = FLT_EPSILON;
	while(1+x!=1) {x/=2;}
	x*=2;
	int a = (x==f);
	printf("float x=%f\n",x);
	printf("x=machine epsilon, true=%i\n",a);

	double y=1;
	double d = DBL_EPSILON;
	while(1+y!=1) {y/=2;}
	y*=2;
	int b = (y==d);
	printf("double y=%g\n",y);
	printf("y=machine epsilon, true=%i\n",b);

	long double z=1;
	long double ld = LDBL_EPSILON;
	while(1+z!=1) {z/=2;}
	z*=2;
	int c = (z==ld);
	printf("long double z=%Lg\n",z);
	printf("z=machine epsilon, true=%i\n",c);




}
