# include <stdio.h>
# include <math.h>
# include <limits.h>

int main(void){
	int j=INT_MIN;

	printf("The result with the while loop:\n");
	int i=1;
	while(i-1<i) {i--;}
	printf("my min int = %i\n",i);
	int a = (i==j);
	printf("i==INT_MIN,%i\n",a);

	printf("The result with the for loop:\n");
	int k;
	for (k=1;k-1<k;k--){}
	printf("my min int = %i\n",k);
	int b = (k==j);	
	printf("k==INT_MIN,%i\n",b);
	
	printf("The result with the do-while loop:\n");
	int l=1;
	do {l--;}
	while (l-1<l);
	printf("my min int = %i\n",l);
	int c = (l==j);	
	printf("l==INT_MIN,%i\n",c);
	



}
