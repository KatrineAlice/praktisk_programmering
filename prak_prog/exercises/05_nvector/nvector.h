#ifndef HAVE_NVECTOR_H

typedef struct {int size; double* data;} nvector;

nvector* nvector_alloc       (int n);                 /* alloc. memory for n-size vec  */
void     nvector_free        (nvector* v);                             /* frees memory */
void     nvector_set         (nvector* v, int i, double value);   /* gives v_i a value */
double   nvector_get         (nvector* v, int i);                       /* returns v_i */
double   nvector_dot_product (nvector* u, nvector* v);          /* returns dot-product */
void     nvector_add         (nvector* a, nvector* b);             /* a_i <- a_i + b_i */
void     nvector_sub         (nvector* a, nvector* b);             /* a_i <- a_i - b_i */
void     nvector_scale       (nvector* a, double x);                   /* a_i <- x*a_i */
void     nvector_set_zero    (nvector* a);             /* all elements are set to zero */
int      double_equal        (double a, double b);                         /* as below */
int      nvector_equal       (nvector* a, nvector* b);      /* 1 if equal, 0 otherwise */
void     nvector_print       (char* s, nvector* v);        /* prints s and then vector */

#define HAVE_NVECTOR_H
#endif
