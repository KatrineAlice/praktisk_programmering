#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"nvector.h"
#include<assert.h>

#ifndef TAU
#define TAU 1e-6
#endif
#ifndef EPS
#define EPS 1e-6
#endif

nvector* nvector_alloc(int n){
	nvector* v = malloc(sizeof(nvector));
 	(*v).size = n;
 	(*v).data = malloc(n*sizeof(double));
	if( v==NULL ) fprintf(stderr,"error in nvector_alloc\n");
	return v;
}

void nvector_free(nvector* v){
	free(v->data);
	free(v);
}

void nvector_set(nvector* v, int i, double value){
	assert(0 <= i && i < (*v).size);
	(*v).data[i]=value;
}

double nvector_get(nvector* v, int i){
	return (*v).data[i];
}

double nvector_dot_product(nvector* u, nvector* v){
	assert(u->size == v-> size);
	double d = 0;
	for (int i = 0; i < u-> size; i++){
		double s = nvector_get(u, i) * nvector_get(v, i);
		d += s;
	}
	return d;
}

void nvector_add(nvector* a, nvector* b){
	assert(a->size == b-> size);
	for (int i = 0; i < a-> size; i++){
		double s = nvector_get(a, i) + nvector_get(b, i);
		nvector_set(a, i, s);
	}
}

void nvector_sub(nvector* a, nvector* b){
	assert(a->size == b-> size);
	for (int i = 0; i < a-> size; i++){
		double s = nvector_get(a, i) - nvector_get(b, i);
		nvector_set(a, i, s);
	}
}

int double_equal(double a, double b){
	if (fabs(a - b) < TAU)
		return 1;
	if (fabs(a - b) / (fabs(a) + fabs(b)) < EPS / 2)
		return 1;
	return 0;
}

int nvector_equal(nvector* a, nvector* b){
	if (a->size != b->size) return 0;
	for (int i = 0; i < a->size; i++)
		if (!double_equal(a->data[i], b->data[i]))
			return 0;
	return 1;
}

void nvector_print(char *s, nvector* v){
	printf("%s", s);
	for (int i = 0; i < v->size; i++)
		printf("%9.3g ", v->data[i]);
	printf("\n");
}

void nvector_set_zero(nvector* v){
	double d = 0;
	for (int i = 0; i < v->size; i++)
		nvector_set(v, i, d);
}

void nvector_scale(nvector* a, double x){
	for (int i = 0; i < a-> size; i++){
		double d = nvector_get(a,i) * x;
		nvector_set(a, i, d);
	}
}
