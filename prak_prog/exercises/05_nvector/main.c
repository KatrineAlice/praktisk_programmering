#include<stdio.h>
#include<math.h>
#include"nvector.h"
#include<stdlib.h>
#define RND (double)rand()/RAND_MAX

int main(){
	int n = 10;

	printf("main: testing nvector_alloc ...\n");
	nvector *v = nvector_alloc(n);
	if (v==NULL) printf("test failed\n");
	else printf("test succeeded\n");

	printf("\nmain: testing nvector_set and nvector_get ...\n");
	double value = RND;
	int i = n/2;
	nvector_set(v, i, value);
	double vi = nvector_get(v, i);
	if (double_equal(vi, value)) printf("test succeeded\n");
	else printf("test failed\n");

	printf("\nmain: testing nvector_add and nvector_print ...\n");
	nvector *a = nvector_alloc(n);
	nvector *b = nvector_alloc(n);
	nvector *c = nvector_alloc(n);
	for (int i = 0; i < n; i++){
		double x = RND, y = RND;
		nvector_set(a, i, x);
		nvector_set(b, i, y);
		nvector_set(c, i, x + y);
	}
	nvector_add(a, b);
	nvector_print("a+b should be = \n", c);
	nvector_print("a+b is really = \n", a);
	if (nvector_equal(c, a)) printf("test succeeded\n");
	else printf("test failed\n");

	printf("\nmain: Testing nvector_set_zero ...\n");
	nvector_set_zero(c);
	nvector_print("c should be all zeroes, but is =\n", c);
	double zero = 0;
	double ci = nvector_get(c, i);
	if (double_equal(ci, zero)) printf("test succeeded\n");
	else printf("test failed\n");
	
	printf("\nmain: Testing nvector_dot_product ...\n");
	nvector *d = nvector_alloc(n);
	nvector *e = nvector_alloc(n);
	double k = 0;
	for (int i=0; i < n; i++){
		double x = RND, y = RND;
		nvector_set(d, i, x);
                nvector_set(e, i, y);
                k += x * y;
	}
	double dot = nvector_dot_product(d, e);
	printf("The dot product of d and e should = %g\n", k);
	printf("But is really = %g\n", dot);
	if (double_equal(k, dot)) printf("test succeeded\n");
	else printf("test failed\n");

	printf("\nmain: Testing nvector_scale ...\n");
	nvector *f = nvector_alloc(n);
	nvector *g = nvector_alloc(n);
	double x = 5;
	for (int i=0; i < n; i++){
		double y = RND;
		nvector_set(f, i, y);
		nvector_set(g, i, x * y);
	}
	nvector_scale(f, x);
	nvector_print("x*f should be = \n", g);
	nvector_print("But is really = \n", f);
	if (nvector_equal(g, f)) printf("test succeeded\n");
	else printf("test failed\n");

	nvector_free(v);
	nvector_free(a);
	nvector_free(b);
	nvector_free(c);
	nvector_free(d);
	nvector_free(e);
	nvector_free(f);
	nvector_free(g);

	return 0;
}
