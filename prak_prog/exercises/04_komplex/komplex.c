#include<stdio.h>
#include<math.h>
#include"komplex.h"

void komplex_print (char *s, komplex a) {
	printf ("%s (%g,%g)\n", s, a.re, a.im);
}

void komplex_set (komplex* z, double x, double y) {
	(*z).re = x;
	(*z).im = y;
}

komplex komplex_new (double x, double y) {
	komplex z = { x, y };
	return z;
}

komplex komplex_add (komplex a, komplex b) {
	komplex result = { a.re + b.re , a.im + b.im };
	return result;
}

komplex komplex_sub (komplex a, komplex b){
	komplex sub = { a.re - b.re , a.im - b.im };
	return sub;
}

komplex komplex_mul (komplex a, komplex b){
	komplex mul = { a.re*b.re - a.im*b.im , a.re*b.im + a.im*b.re };
	return mul;
}

komplex komplex_div (komplex a, komplex b){
	komplex div = {.re=(a.re*b.re + a.im*b.im)/(b.re*b.re + b.im*b.im) ,.im=(a.im*b.re - a.re*b.im)/(b.re*b.re + b.im*b.im) };
	return div;
}

komplex komplex_conjugate (komplex z){
	komplex conjugate = { z.re , -z.im };
	return conjugate;
}

double komplex_abs (komplex z){
	double a = sqrt(z.re*z.re + z.im*z.im);
	return a;
}

komplex komplex_exp (komplex z){
	komplex a;
	a.re = cos(z.im) * exp(z.re);
	a.im = sin(z.im) * exp(z.re);
	return a;
}



/* Not included here or in .h file: equal, sin, cos, sqrt */
