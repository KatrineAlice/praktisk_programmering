Questions 1

1) What is the main function?

In every program in c there must be a main function. This can then call to
other functions in the program. 

2) Compilation and linking? Source, object and executable code? 

The source code is the code written "for a human to understand".
The object code is the code written "for a processor to understand".
An the executabe code is the code we can run as a program. 

If we only compile we can get an object code but when run through fx gcc
it will produce the executable code that kan then be run by accessing it. 
By default (historical reasons) this i called a.out (run by ./a.out)

If a program consists of multiple codes/functions it needs to be linked,
fx main.c and hello.c. If we first compile them to objects by 
gcc -c main.c -o main.o (likewise for hello.c) then we can link them through
gcc main.o hello.o. They can actually be linkes as .c files as well, but 
for large projects this is much slower, since all files must be compiled every
time one file is changed/updated. Therefore: compile to .o first. 

3) Compile/linking of a c-program contained in one file, hello.c

If everything is contained in one file, it is run by cc (c compiler) or gcc 
(gnu c compiler). The resulting executable code is run by ./a.out

4) supply the compiler with the description of printf. Why? Link?

printf lies in the standard in out library and must be called at 
			#include <stdio.h> 
unix also has a printf function, but it doesn't do the same! We don't 
want that one. 

Link: since it is already a standard library it doesn't have to be linked,
as fx math.h does.

5) stdio.h? Why angle brackets? 

See above. It is a header file that fx contains the printf function. It is
not the whole library. The < >  brackets indicate that is is within the system.
If it was our own we would write " " instead.

6) Manual for correct printf?

man 3 printf
since section 3 always indicate libraries
If a number is AFTER fx man 3 printf 2, it would be page 2 in section 3. 

7) built-in data types?

int		integers 
float		digits
double		more digits, use this most often
longdouble	many digits, but makes it slower
complex		complex numbers, needs complex.h

8) Result of operation 1/2 in C?

Depends on input/output. 
integer returns 0
double returns decimal. An likewise with the others.

9) C and undeclared variable? undeclared function? 
Can a variable hold different types?

No variables and functions must be declared.

In doubt if it can hold multiple types. Probably not? Casting, fx first
defining it as void (but if you're sure of number then int) and the later
casting it to another type, fx double.

10) mathematical functions in C, header files? Library linking?

Yes, #include<math.h>, if complex #include<complex.h> since for historical
reasons this wasn't included in the math. 

The sin function is dependent on type:
double		sin	csin
float		sinf	csinf
longdouble	sinl	csinl

and -lm is needed to link to the math library.

11) tgmath.h? Library?

tgmath.h is type generic math and it preprocesses the code so sin will assign 
itself to the correct type. 
We still have to use -lm when compiling.

12) pow function? Complex cpow?  

Power function. Used in same way as the sine.
(of course it takes two arguments)
the complex doesn't contain the complex i (I)
