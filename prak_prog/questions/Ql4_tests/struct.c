#include<stdio.h>
struct komplex {double re; double im;};
const struct komplex I = {0,1};
typedef struct komplex komplex;
const komplex J = {.re=0, .im=1};
int main(){
	komplex z;
	z.re=1;
	z.im=2;
	printf("z=(%g,%g)\n",z.re,z.im);
	printf("I=(%g,%g)\n",I.re,I.im);
	printf("J=(%g,%g)\n",J.re,J.im);
return 0;
}
